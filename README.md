<style>
div.center {
text-align: center;
}
</style>





# RDV
## <small>Prise de rendez-vous. - Making appointments.</small>

<div class="center" align="center">

![](rdv/assets/images/logo.png)
</div>

<table><tr>
<td>
RDV a été développé dans le but de simplifier la prise de rendez-vous pour les réunions parents-profs.
<br />C'est un logiciel libre (licence [GNU GPL](https://www.gnu.org/copyleft/gpl.html)) qui fonctionne en ligne.
</td><td>
RDV has been developed to simplify the booking of appointments for parent-teacher meetings.
<br />It is a free software (license [GNU GPL](https://www.gnu.org/copyleft/gpl.html)) that works online.
</td>
</tr></table>





----

* **Website:** http://pascal.peter.free.fr/rdv.html
* **Email:** pascal.peter at free.fr
* **License:** GNU General Public License (version 3)
* **Copyright:** (c) 2022





----

### Les outils utilisés pour développer RDV - The tools used to develop RDV

* Langages de programmation - Programming languages
    * [JavaScript](https://fr.wikipedia.org/wiki/JavaScript)
    * [PHP](https://fr.wikipedia.org/wiki/PHP)
* Pages réalisées avec - Pages made with
    * [Markdown](https://daringfireball.net/projects/markdown)
    * [Pandoc](http://www.pandoc.org)
    * [Bootstrap](https://getbootstrap.com) + [Bootstrap Icons](https://icons.getbootstrap.com/)
    * [Breeze-icons theme](https://invent.kde.org/frameworks/breeze-icons)
* Logo et favicon réalisés avec - Logo and favicon made with
    * [Blender](https://www.blender.org)
    * [RealFaviconGenerator](https://realfavicongenerator.net/)





----

### Installer RDV - Install RDV

<table><tr>
<td>
Il suffit de déposer le dossier <b>rdv</b> sur votre site web.
<br />Des utilisateurs (comptes profs et élèves) existent déjà pour tester l'application :
</td><td>
Simply upload the <b>rdv</b> file to your website.
<br />Users (teacher and student accounts) already exist to test the application:
</td>
</tr></table>

<div class="center" align="center">

![](users/images/CREATE_USERS-13.png) ![](users/images/CREATE_USERS-12.png)
</div>

<table><tr>
<td>
Pour modifier la liste des utilisateurs, voir le fichier [users/CREATE_USERS](users/CREATE_USERS.md).
</td><td>
To modify the list of users, see the file [users/CREATE_USERS](users/CREATE_USERS.md).
</td>
</tr></table>






