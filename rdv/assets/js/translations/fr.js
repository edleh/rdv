/**
#-----------------------------------------------------------------
# This file is a part of RDV project.
# Copyright:    (C) 2022 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


const TRANSLATIONS = {

    // page index
    'login': 'connexion', 
    'Login': 'Identifiant', 
    'Password': 'Mot de passe', 
    'Sign in': 'Se connecter', 
    'Incorrect login or password!': 'Identifiant ou mot de passe incorrect !', 

    // page user
    'user': 'utilisateur', 
    'APPOINTMENT BOOKING': 'PRISE DE RENDEZ-VOUS', 
    'Remember to save your changes before leaving (button at the bottom of this page).': 
        'Pensez à enregistrer vos modifications avant de quitter (bouton situé en bas de cette page).', 
    'You can write a message below': 'Vous pouvez écrire un message ci-dessous', 
    'Choose a time slot for the appointment': 'Choisissez un créneau pour le rendez-vous', 
    'Date': 'Date', 
    'Referring teacher': 'Professeur référent', 
    'No appointment desired': 'Pas de RDV souhaité', 

    // page user-confirmation
    'confirmation': 'confirmation', 
    'Confirmation of your appointment': 'Confirmation de votre rendez-vous', 
    'CONFIRMATION OF YOUR APPOINTMENT': 'CONFIRMATION DE VOTRE RENDEZ-VOUS', 
    'Your appointment with': 'Votre rendez-vous avec', 
    'is scheduled on': 'est fixé le', 
    'on site.': 'en présence.', 
    'by phone.': 'par téléphone.', 
    'You do not want an appointment with': 'Vous ne souhaitez pas de rendez-vous avec', 
    "Teacher's message": 'Message du professeur', 
    'Your message': 'Votre message', 

    // page manager
    'Home': 'Accueil', 
    'manager': 'gestion', 
    'MANAGEMENT OF APPOINTMENTS': 'GESTION DES RENDEZ-VOUS', 
    'Students': 'Élèves', 
    'Selection of students': 'Sélectionner les élèves', 
    'Time slots': 'Créneaux', 
    'Manage available time slots': 'Gérer les créneaux horaires disponibles', 
    //'Your message': 'Votre message', 
    'Define a message for parents': "Définir un message pour les parents d'élèves", 
    'Appointments': 'Rendez-vous', 
    'See the appointments taken': 'Voir les rendez-vous pris', 
    'Edit': 'Modifier', 
    'Create or edit appointments': 'Créer ou modifier des rendez-vous', 
    'Confirmations': 'Confirmations', 
    'Print appointment confirmations': 'Imprimer des confirmations de rendez-vous', 
    'Clean': 'Nettoyer', 
    'Delete appointments': 'Supprimer des rendez-vous', 
    'List of students': 'Liste des élèves', 
    'Message': 'Message', 
    //'Date': 'Date', 

    // page manage-users
    'users': 'élèves', 
    'SELECTION OF STUDENTS': 'SÉLECTION DES ÉLÈVES', 
    'List of selected students': 'Liste des élèves sélectionnés', 
    'All classes': 'Toutes les classes', 

    // page manage-timeslots
    'timeslots': 'créneaux horaires', 
    'TIME SLOTS': 'CRÉNEAUX HORAIRES', 
    'Explanations': 'Explications', 
    'Indicate your time slots in the text box below;': 
        'Indiquez vos créneaux horaires dans la zone de texte ci-dessous ;', 
    'each line allows you to create one or more slots;': 
        'chaque ligne permet de créer un ou plusieurs créneaux ;', 
    'you can specify at the end of the line if the appointment will be in presence (': 
        'vous pouvez préciser à la fin de la ligne si le rendez-vous sera en présence (', 
    '), by phone (': '), par téléphone (', 
    ") or at the parents' choice (": ') ou au choix des parents (', 
    'By default the appointment will be available in presence only;': 
        'Par défaut le rendez-vous sera disponible en présence seulement ;', 
    'example lines:': 'exemples de lignes :', 
    ': will create an in-presence appointment slot on Monday 01/03/2022 at 4:00pm': 
        ' : créera un créneau de RDV en présence le lundi 03/01/2022 à 16h00', 
    ': will create a phone appointment on Monday 01/03/2022 at 17h00': 
        ' : créera un créneau de RDV téléphonique le lundi 03/01/2022 à 17h00', 
    ': will create an appointment slot on Monday 01/03/2022 at 17:30 for parents to choose from (phone or on site)': 
        ' : créera un créneau de RDV le lundi 03/01/2022 à 17h30 au choix des parents (tél ou sur place)', 
    ': will create 3 slots on Tuesday 01/04/2022 at the times indicated': 
        ' : créera 3 créneaux le mardi 04/01/2022 aux horaires indiqués', 
    ': will create slots every quarter hour between 4:00pm and 7:00pm.': 
        ' : créera des créneaux tous les quarts d’heure entre 16h et 19h.', 

    // page manage-message
    'message': 'message', 
    'MESSAGE TO USERS': 'MESSAGE AUX UTILISATEURS', 
    //'Explanations': 'Explications', 
    'You can write a message to parents below.': 
        "Vous pouvez écrire ci-dessous un message à l’intention des parents d'élèves.", 
    'The second input field allows you to write a different message for appointment confirmations.': 
        "La deuxième zone de saisie vous permet d'écrire un message différent pour les confirmations de rendez-vous.", 
    'If you leave it empty the first message will be used.': 
        'Si vous la laissez vide le premier message sera utilisé.', 
    'Changes saved.': 'Modifications enregistrées.', 

    // page manage-appointments
    'appointments': 'rendez-vous', 
    'YOUR APPOINTMENTS': 'VOS RENDEZ-VOUS', 
    'Appointments (per student)': 'Rendez-vous pris (par élève)', 
    'Appointments (per date)': 'Rendez-vous pris (par date)', 
    //'No appointment desired': 'Pas de RDV souhaité', 
    'No response': 'Pas de réponse', 

    // page manage-edit
    'edit': 'modifier', 
    'CREATE OR EDIT APPOINTMENTS': 'CRÉER OU MODIFIER DES RENDEZ-VOUS', 
    'This page allows you to enter appointments for parents.': 
        'Cette page vous permet de saisir des rendez-vous à la place des parents.', 
    'Select a student, edit their appointment and save with the button at the bottom of the page.': 
        'Sélectionnez un élève, modifiez son rendez-vous et enregistrez avec le bouton situé au bas de la page.', 
    'Student': 'Élève', 

    // page manage-confirmations
    'confirmations': 'confirmations', 
    'APPOINTMENT CONFIRMATIONS': 'CONFIRMATIONS DES RENDEZ-VOUS', 
    'Print this page to get confirmations to distribute.': 
        'Imprimez cette page pour obtenir des confirmations à distribuer.', 
    //'CONFIRMATION OF YOUR APPOINTMENT': 'CONFIRMATION DE VOTRE RENDEZ-VOUS', 
    'Student:': 'Élève :', 
    'Madam, Sir,': 'Madame, Monsieur,', 
    'your appointment with': 'votre rendez-vous avec', 
    //'is scheduled on': 'est fixé le', 
    //'on site.': 'en présence.', 
    //'by phone.': 'par téléphone.', 
    //"Teacher's message": 'Message du professeur', 
    //'Your message': 'Votre message', 

    // page manage-clean
    'clean': 'nettoyer', 
    'DELETE APPOINTMENTS': 'SUPPRIMER DES RENDEZ-VOUS', 
    //'Explanations': 'Explications', 
    'Select the appointments you want to delete and click on the "Clean" button.': 
        'Sélectionnez les rendez-vous que vous voulez supprimer puis cliquez sur le bouton "Nettoyer".', 
    //'No appointment desired': 'Pas de RDV souhaité', 
    //'Clean': 'Nettoyer', 
    'Delete selected appointments': 'Supprimer les rendez-vous sélectionnés', 
    'Select All': 'Sélectionner tout', 
    'Select all appointments': 'Sélectionner tous les rendez-vous', 

    // page password
    'password': 'mot de passe', 
    'CHANGE PASSWORD': 'CHANGER DE MOT DE PASSE', 
    'New password:': 'Nouveau mot de passe :', 
    'Confirmation:': 'Confirmation :', 
    'The new password does not match the confirmation.': 
        'Le nouveau mot de passe ne correspond pas à la confirmation.', 
    'The password has been changed.': 'Le mot de passe a été changé.', 
    'There was a problem.<br />The change could not be performed.': 
        "Il y a eu un problème.<br />Le changement n'a pas pu être effectué.", 

    // utilisé dans plusieurs pages
    'QUIT': 'QUITTER', 
    'HELP': 'AIDE', 
    'BACK': 'RETOUR', 
    //'CHANGE PASSWORD': 'CHANGER DE MOT DE PASSE', 

    'Sunday': 'Dimanche', 
    'Monday': 'Lundi', 
    'Tuesday': 'Mardi', 
    'Wednesday': 'Mercredi', 
    'Thursday': 'Jeudi', 
    'Friday': 'Vendredi', 
    'Saturday': 'Samedi', 

    'Save': 'Enregistrer', 
    'Save changes': 'Enregistrer les modifications', 

    'Website': 'Site web', 

    '': '', 
    };


function tr(text) {
    let result = TRANSLATIONS[text.trim()];
    if (result === undefined)
        result = text;
    else if (result == '')
        result = text;
    //console.log('TR : ' + text + ' -> ' + result);
    return result;
    }

