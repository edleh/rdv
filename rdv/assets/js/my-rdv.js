/**
#-----------------------------------------------------------------
# This file is a part of RDV project.
# Copyright:    (C) 2022 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/




/* **********************************************
    RÉCUPÉRATION DE LA PAGE
********************************************** */

let PAGE = window.location.href.split('/');
PAGE = PAGE[PAGE.length - 1].split('.')[0];
if (PAGE == '')
    PAGE = 'index';
//console.log('page :' + PAGE);




/* **********************************************
    VARIABLES GLOBALES
********************************************** */

const APPNAME = 'RDV', 
    LOCAL_URL = 'assets/script.php', 
    PAGES_WITH_SAVE_BTN = ['manage-users', 'manage-timeslots', 'manage-message', 'manage-edit', 'user', 'password'], 
    WEEKDAY = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'], 
    ONE_MIN_TIME = 60000, 
    ONE_DAY_TIME = 86400000, 
    CONTENT_TYPE = 'application/x-www-form-urlencoded';

let SESSION = {
    // pour tous :
    //'protocol': '', 
    'hostname': '', 
    'mustLoadData': true, 
    'user': '|-1|-1||||', 
    'userName': '', 
    'modified': false, 
    'temp': [], 
    // pour les profs seulement :
    'profUsers': '', 
    'profTimeslotsText': '', 
    'profMessage': '', 
    'profConfirmation': '', 
    'profAppointments': [], 
    // pour les parents seulement :
    'userData': {'profId': -1, 'profs': [], }, 
    'userChoice': {}, 
    'userAppointments': {}, 
    };






























/* **********************************************
    FONCTIONS UTILES
********************************************** */

function replaceBadChars(text, sens=0) {
    // remplacement des caractères spéciaux
    let result = text;
    if (sens < 1) {
        // on encode
        result = result.split('&').join('£££$$$');
        result = result.split('|').join('££$$');
        result = result.split('\n').join('£$');
        }
    else if (sens == 2) {
        // on restaure avec des BR (pour mettre dans un alert)
        result = result.split('£££$$$').join('&');
        result = result.split('££$$').join('|');
        result = result.split('£$').join('<br />');
        }
    else {
        // on restaure
        result = result.split('£££$$$').join('&');
        result = result.split('££$$').join('|');
        result = result.split('£$').join('\n');
        }
    return result;
    }


function setHeight(el, val) {
    // fixe la hauteur d'un élément
    if (typeof val === "function")
        val = val();
    if (typeof val === "string")
        el.style.height = val;
    else
        el.style.height = val + "px";
}


function formatNumber(n) {
    // force l'écriture avec 2 chiffres (pour les dates)
    if (n < 10)
        return '0' + n;
    else
        return n;
    }


function timestampToText(timestamp) {
    // transforme le nombre "202201041645" en texte "Mardi 04/01/2022 - 16h45"
    let aaaa, mm, jj, hh, min;
    let result = timestamp;
    aaaa = Math.floor(result / 100000000);
    result = result - aaaa*100000000;
    mm = Math.floor(result / 1000000);
    result = result - mm*1000000;
    jj = Math.floor(result / 10000);
    result = result - jj*10000;
    hh = Math.floor(result / 100);
    min = result - hh*100;
    result = tr(WEEKDAY[new Date(aaaa, mm - 1, jj).getDay()]) 
        + ' ' + formatNumber(jj) + '/' + formatNumber(mm) + '/' + formatNumber(aaaa) 
        + ' - ' + formatNumber(hh) + 'h' + formatNumber(min);
    //console.log('timestampToText : ' + timestamp + ' > ' + result);
    return result;
    }

function textToTimestamp(text) {
    // transforme le texte "04/01/2022-16h45" en nombre "202201041645"
    let date, horaire;
    let result = text.split('-');
    date = result[0].split('/');
    horaire = result[1].split('h');

    result = Number(date[2]) * 100000000 
        + Number(date[1]) * 1000000 
        + Number(date[0]) * 10000 
        + Number(horaire[0]) * 100 
        + Number(horaire[1]);

    //console.log('textToTimestamp : ' + text + ' > ' + result);
    return result;
    }

function textToDate(text) {
    // transforme le texte "04/01/2022-16h45" en date javascript
    let date, horaire;
    let result = text.split('-');
    date = result[0].split('/');
    horaire = result[1].split('h');
    result = new Date(date[2], date[1] - 1, date[0], horaire[0], horaire[1]);
    //console.log('textToDate : ' + text + ' > ' + result);
    return result;
    }

function dateToTimestamp(date) {
    // transforme une date javascript en nombre "202201041645"
    let result = Number(date.getFullYear()) * 100000000 
        + Number(date.getMonth() + 1) * 1000000 
        + Number(date.getDate()) * 10000 
        + Number(date.getHours()) * 100 
        + Number(date.getMinutes());
    //console.log('dateToTimestamp : ' + date + ' > ' + result);
    return result;
    }

function timestampToDate(timestamp, hhmin=false) {
    // transforme le nombre "202201041645" en date javascript
    let aaaa, mm, jj, hh, min;
    let result = timestamp;
    aaaa = Math.floor(result / 100000000);
    result = result - aaaa*100000000;
    mm = Math.floor(result / 1000000);
    result = result - mm*1000000;
    jj = Math.floor(result / 10000);
    result = result - jj*10000;
    hh = Math.floor(result / 100);
    min = result - hh*100;
    if (hhmin)
        result = new Date(aaaa, mm - 1, jj, hh, min);
    else
        result = new Date(aaaa, mm - 1, jj);
    //console.log('timestampToDate : ' + timestamp + ' > ' + result);
    return result;
    }


function saveSession() {
    if (localStorage) {
        let json = JSON.stringify(SESSION);
        sessionStorage.setItem(APPNAME, json);
        };
    }






























/* **********************************************
    FONCTIONS SPÉCIFIQUES À CERTAINES PAGES
********************************************** */

function updateNavBar() {
    /*
    mise à jour des boutons de la barre de titre.
    On en profite pour gérer le bouton "Enregistrer"
    */
    document.getElementById('userName').innerHTML = SESSION['userName'];
    document.getElementById('btn-quit').setAttribute('title', tr('QUIT'));
    document.getElementById('btn-help').setAttribute('title', tr('HELP'));
    if ((PAGE == 'user') || (PAGE == 'user-confirmation'))
        document.getElementById('btn-help').setAttribute('href', 'http://pascal.peter.free.fr/rdv.html#parents');
    document.getElementById('btn-back').setAttribute('title', tr('BACK'));
    document.getElementById('btn-password').setAttribute('title', tr('CHANGE PASSWORD'));
    if (!(SESSION['hostname'].includes('clgdrouyn')))
        document.getElementById('btn-password').classList.toggle("d-none");
    if (PAGES_WITH_SAVE_BTN.indexOf(PAGE) != -1) {
        document.getElementById('btn-save').innerHTML = tr('Save');
        document.getElementById('btn-save').setAttribute('title', tr('Save changes'));
        document.getElementById('btn-save').addEventListener('click', save, false);
        }
    if (PAGE.substring(0, 6) == 'manage') {
        let buttons = [
            ['manager', 'Home', 'go-home'], 
            ['manage-users', 'Selection of students', 'users'], 
            ['manage-timeslots', 'Manage available time slots', 'clock'], 
            ['manage-message', 'Define a message for parents', 'message'], 
            ['manage-appointments', 'See the appointments taken', 'calendar'], 
            ['manage-edit', 'Create or edit appointments', 'edit'], 
            ['manage-confirmations', 'Print appointment confirmations', 'print'], 
            ['manage-clean', 'Delete appointments', 'clear'], 
            ];
        let html = '';
        for (let i = 0; i < buttons.length; i++) {
            if (buttons[i][0] == PAGE)
                html += '<a class="navbar-brand disabled" ';
            else
                html += '<a class="navbar-brand filter-white" ';
            html += 'href="' + buttons[i][0] + '.html" ';
            html += 'title="' + tr(buttons[i][1]) + '">';
            html += '<img src="assets/images/' + buttons[i][2] + '.svg" width="24"></a>';
            }
        //document.getElementById('btn-back').style.display = 'none';
        document.getElementById('navbar-buttons').innerHTML = html;
        }
    }


function classChange() {
    // combobox des classes (page manage-users)
    let className = document.getElementById("select-class").value;
    if (className == tr('All classes')) {
        [].forEach.call(document.querySelectorAll('.list-group-item'), function(el) {
            el.style.display = '';
            });
        }
    else {
        [].forEach.call(document.querySelectorAll('.list-group-item'), function(el) {
            let what = el.getAttribute('what');
            if (what == className)
                el.style.display = '';
            else
                el.style.display = 'none';
            });
        }
    }


function studentChange() {
    // combobox des élèves (page manage-edit)
    SESSION['temp'] = [];
    createTimeslotsTable(who='edit');
    }


function profChange() {
    // combobox des profs (page user)
    let profId = document.getElementById("select-prof").value;
    SESSION['userData']['profId'] = profId;
    loadUserData();
    }


function createTimeslotsTable(who='') {
    /*
    mise en place du tableau des créneaux de RDV 
    (pages manager, manage-timeslots, manage-edit et user)
    */
    let result = [], reserveds = [], checked = '';
    let timeslots;

    if (who == 'user') {
        let profId = SESSION['userData']['profId'];
        timeslots = SESSION['userData'][profId][3].replace(/ /g, '').split('£$');
        for (let i = 0; i < SESSION['userAppointments'][profId].length; i++) {
            let userAppointment = SESSION['userAppointments'][profId][i];
            if ((userAppointment != 0) && (userAppointment != SESSION['userChoice'][profId][2]))
                reserveds.push(parseInt(userAppointment.split('@')[0]));
            }
        }
    else {
        timeslots = SESSION['profTimeslotsText'].split('£$');
        for (let i = 0; i < SESSION['profAppointments'].length; i++) {
            if (SESSION['profAppointments'][i].split('|')[1] != 0)
                reserveds.push(parseInt(SESSION['profAppointments'][i].split('|')[1].split('@')[0]));
            }
        }
    //console.log('reserveds : ' + reserveds);

    for (let i = 0; i < timeslots.length; i++) {
        let temp, type, date;
        let timestamp, timeslot, horaire, day;
        temp = timeslots[i].split('@');
        if (temp.length > 1)
            type = temp[1];
        else
            type = '';
        temp = temp[0].split('-');
        date = temp[0];
        if (temp.length > 1) {
            if (temp[1].indexOf(':') != -1) {
                let begin, end, current, duration;
                begin = textToDate(date + '-' + temp[1].split(':')[0]);
                end = textToDate(date + '-' + temp[1].split(':')[1]);
                temp = temp[2].split('h');
                duration = Number(temp[0]) * 60 + Number(temp[1]);
                temp = [];
                temp.push(begin);
                current = new Date(begin.getTime() + duration * ONE_MIN_TIME);
                while (current < end) {
                    temp.push(current);
                    current = new Date(current.getTime() + duration * ONE_MIN_TIME);
                    }
                temp.push(end);
                for (let j = 0; j < temp.length; j++) {
                    timestamp = dateToTimestamp(temp[j]);
                    timeslot = timestampToText(timestamp);
                    day = timestampToDate(timestamp).getTime();
                    if (reserveds.indexOf(timestamp) != -1)
                        checked = '✗';
                    else
                        checked = '';
                    if (type == 'P')
                        result.push([timeslot, '✗', '', checked, timestamp, day]);
                    else if  (type == 'T')
                        result.push([timeslot, '', '✗', checked, timestamp, day]);
                    else if  (type == 'C')
                        result.push([timeslot, '✗', '✗', checked, timestamp, day]);
                    else
                        result.push([timeslot, '✗', '', checked, timestamp, day]);
                    };
                }
            else
                for (let j = 1; j < temp.length; j++) {
                    horaire = temp[j];
                    timestamp = textToTimestamp(date + '-' + horaire);
                    timeslot = timestampToText(timestamp);
                    day = timestampToDate(timestamp).getTime();
                    if (reserveds.indexOf(timestamp) != -1)
                        checked = '✗';
                    else
                        checked = '';
                    if (type == 'P')
                        result.push([timeslot, '✗', '', checked, timestamp, day]);
                    else if  (type == 'T')
                        result.push([timeslot, '', '✗', checked, timestamp, day]);
                    else if  (type == 'C')
                        result.push([timeslot, '✗', '✗', checked, timestamp, day]);
                    else
                        result.push([timeslot, '✗', '', checked, timestamp, day]);
                    };
            }
        
        }
    //console.log(result);
    let html = '', 
        line, 
        before = '<input class="form-check-input" type="radio" name="choix" value="', 
        after = '" onclick="selectTimeslot(this);"', 
        choice, choiceDay, 
        mustDisable, disabled = '';
    let now = dateToTimestamp(new Date());
    now = timestampToDate(now).getTime();
    //console.log('now : ' + now);

    if (who == 'user') {
        let profId = SESSION['userData']['profId'];
        choice = SESSION['userChoice'][profId][2].split('@');
        choiceDay = timestampToDate(choice[0]).getTime();
        //console.log('choiceDay : ' + choiceDay);
        }
    else if (who == 'edit') {
        let studentId = document.getElementById("select-student").value;
        choice = [-1, 0];
        for (let i = 0; i < SESSION['profAppointments'].length; i++) {
            if (SESSION['profAppointments'][i].split('|')[0] == studentId)
                choice = SESSION['profAppointments'][i].split('|')[1].split('@');
            }
        }

    html = '';
    for (let i = 0; i < result.length; i++) {
        if (who == 'user') {

            mustDisable = false;
            disabled = '';
            if (result[i][5] < now + ONE_DAY_TIME)
                mustDisable = true;
            else if (reserveds.indexOf(result[i][4]) != -1)
                mustDisable = true;
            if (mustDisable) {
                result[i][0] = '<del class="text-muted">' + result[i][0] + '</del>';
                disabled = ' disabled';
                }

            checked = '';
            if (result[i][1] != '') {
                if ((result[i][4] == choice[0]) && (choice[1] == 'P'))
                    checked = ' checked';
                else
                    checked = '';
                result[i][1] = before + result[i][4] + '@P' + after + checked + disabled + '>';
                }
            checked = '';
            if (result[i][2] != '') {
                if ((result[i][4] == choice[0]) && (choice[1] == 'T'))
                    checked = ' checked';
                result[i][2] = before + result[i][4] + '@T' + after + checked + disabled + '>';
                }

            line = '<tr><td>' + result[i][0] 
                + '</td><td>' + result[i][1] 
                + '</td><td>' + result[i][2] 
                + '</td></tr>';
            }
        else if (who == 'edit') {
            mustDisable = false;
            disabled = '';
            if ((reserveds.indexOf(result[i][4]) != -1) && (result[i][4] != choice[0]))
                mustDisable = true;
            if (mustDisable) {
                result[i][0] = '<del class="text-muted">' + result[i][0] + '</del>';
                disabled = ' disabled';
                }

            checked = '';
            if (result[i][1] != '') {
                if ((result[i][4] == choice[0]) && (choice[1] == 'P'))
                    checked = ' checked';
                else
                    checked = '';
                result[i][1] = before + result[i][4] + '@P' + after + checked + disabled + '>';
                }
            checked = '';
            if (result[i][2] != '') {
                if ((result[i][4] == choice[0]) && (choice[1] == 'T'))
                    checked = ' checked';
                result[i][2] = before + result[i][4] + '@T' + after + checked + disabled + '>';
                }

            line = '<tr><td>' + result[i][0] 
                + '</td><td>' + result[i][1] 
                + '</td><td>' + result[i][2] 
                + '</td></tr>';
            }
        else
            line = '<tr><td>' + result[i][0] 
                + '</td><td>' + result[i][1] 
                + '</td><td>' + result[i][2] 
                + '</td><td>' + result[i][3] 
                + '</td></tr>';
        html += line;
        }
    //console.log(html);
    document.getElementById('timeslots').innerHTML = html;

    if (who == 'user') {
        html = '<input class="form-check-input" type="radio" ';
        html += 'name="choix" value="0" onclick="selectTimeslot(this);"';
        if (choice[0] == 0)
            html += ' checked>';
        else
            html += '>';
        html += '<label class="form-check-label">' 
            + tr('No appointment desired') + '</label>';
        document.getElementById('user-noRDV').innerHTML = html;
        }
    else if (who == 'edit') {
        html = '<input class="form-check-input" type="radio" ';
        html += 'name="choix" value="0" onclick="selectTimeslot(this);"';
        if (choice[0] == 0)
            html += ' checked>';
        else
            html += '>';
        html += '<label class="form-check-label">' 
            + tr('No appointment desired') + '</label>';
        document.getElementById('user-noRDV').innerHTML = html;
        }
    }


function selectTimeslot(element) {
    // sélection d'un créneau (pages manage-edit et user)
    if (PAGE == 'manage-edit') {
        let studentId = document.getElementById("select-student").value;
        SESSION['temp'] = [studentId, element.value];
        saveSession();
        }
    else if (PAGE == 'user') {
        let profId = SESSION['userData']['profId'];
        SESSION['userChoice'][profId][2] = element.value;
        SESSION['modified'] = true;
        saveSession();
        }
    }


function messageChange() {
    //console.log('messageChange');
    SESSION['modified'] = true;
    saveSession();
    }


function updateAppointmentsPage() {
    // page manage-appointments
    let now = new Date();
    now = dateToTimestamp(now);
    now = timestampToText(now);
    document.getElementById('date').innerHTML = now;

    let temp, timestamp;
    let who, whoId, whoName;
    let DATA = {
        'appointments': [], 
        'noAppointment': [], 
        'noResponse': [], 
        'USERS': {}, 
        'TIMESLOTS': {}, 
        'sortedByUsers': [], 
        'sortedByTimeslots': [], 
        };
    for (let i = 0; i < SESSION['profAppointments'].length; i++) {
        temp = SESSION['profAppointments'][i].split('|');
        whoId = temp[0];
        DATA['USERS'][whoId] = [temp[1], temp[2]];
        if (parseInt(temp[1].split('@')[0]) == 0)
            DATA['noAppointment'].push(whoId);
        else {
            DATA['appointments'].push(whoId);
            timestamp = temp[1].split('@')[0];
            if (timestamp in DATA['TIMESLOTS'])
                DATA['TIMESLOTS'][timestamp].push(whoId);
            else
                DATA['TIMESLOTS'][timestamp] = [whoId];
            DATA['sortedByTimeslots'].push(timestamp);
            }
        }
    DATA['sortedByTimeslots'].sort();
    for (let i = 0; i < SESSION['profUsers']['linkedUsers'].length; i++) {
        whoId = SESSION['profUsers']['linkedUsers'][i];
        if ((DATA['appointments'].indexOf(whoId) == -1) && (DATA['noAppointment'].indexOf(whoId) == -1)) {
            DATA['noResponse'].push(whoId);
            DATA['USERS'][whoId] = ['', ''];
            }
        else if (DATA['appointments'].indexOf(whoId) > -1)
            DATA['sortedByUsers'].push(whoId);
        }
    for (let i = 0; i < SESSION['profUsers']['allUsers'].length; i++) {
        who = SESSION['profUsers']['allUsers'][i].split('&');
        whoName = who[1] + ' ' + who[2] + ' (' + who[3] + ')';
        if (SESSION['profUsers']['linkedUsers'].indexOf(who[0]) != -1)
            DATA['USERS'][who[0]].push(whoName);
        }

    let html = '', htmlPrint = '';
    html = '<div class="d-print-none"><ul>';
    htmlPrint = '<div class="d-none d-print-block"><ul>';
    for (let i = 0; i < DATA['sortedByUsers'].length; i++) {
        who = DATA['USERS'][DATA['sortedByUsers'][i]];
        html += '<li><b>' + who[2] + '</b>';
        htmlPrint += '<li><b>' + who[2] + '</b>';
        temp = who[0].split('@');
        if (temp[1] == 'T') {
            html += '<br />' + timestampToText(temp[0]) + ' 📞';
            htmlPrint += ' --- ' + timestampToText(temp[0]) + ' 📞';
            }
        else {
            html += '<br />' + timestampToText(temp[0])  + ' 👁';
            htmlPrint += ' --- ' + timestampToText(temp[0])  + ' 👁';
            }
        if (who[1].length > 0) {
            html += '<br />';
            html += '<div class="alert alert-secondary">' 
                    + replaceBadChars(who[1], sens=2) + '</div>';
            htmlPrint += '<br />';
            htmlPrint += '<div class="alert alert-secondary">' 
                    + replaceBadChars(who[1], sens=2) + '</div>';
            }
        html += '</li>';
        htmlPrint += '</li>';
        }
    html += '</ul></div>';
    htmlPrint += '</ul></div>';
    document.getElementById('list-appointments-users').innerHTML = html + htmlPrint;

    html = '<div class="d-print-none"><ul>';
    htmlPrint = '<div class="d-none d-print-block"><ul>';
    for (let i = 0; i < DATA['sortedByTimeslots'].length; i++) {
        timestamp = DATA['sortedByTimeslots'][i];
        temp = DATA['TIMESLOTS'][timestamp].shift();
        who = DATA['USERS'][temp];
        temp = who[0].split('@');
        if (temp[1] == 'T') {
            html += '<li><b>' + timestampToText(temp[0]) + '</b>' + ' 📞';
            htmlPrint += '<li><b>' + timestampToText(temp[0]) + '</b>' + ' 📞';
            }
        else {
            html += '<li><b>' + timestampToText(temp[0]) + '</b>' + ' 👁';
            htmlPrint += '<li><b>' + timestampToText(temp[0]) + '</b>' + ' 👁';
            }
        html += '<br />' + who[2];
        htmlPrint += ' --- ' + who[2];
        if (who[1].length > 0) {
            html += '<br />';
            html += '<div class="alert alert-secondary">' 
                    + replaceBadChars(who[1], sens=2) + '</div>';
            htmlPrint += '<br />';
            htmlPrint += '<div class="alert alert-secondary">' 
                    + replaceBadChars(who[1], sens=2) + '</div>';
            }
        html += '</li>';
        htmlPrint += '</li>';
        }
    html += '</ul></div>';
    htmlPrint += '</ul></div>';
    document.getElementById('list-appointments-date').innerHTML = html + htmlPrint;

    html = '<ul>';
    for (let i = 0; i < DATA['noAppointment'].length; i++) {
        who = DATA['USERS'][DATA['noAppointment'][i]];
        html += '<li>' + who[2];
        if (who[1].length > 0) {
            html += '<br />';
            html += '<div class="alert alert-secondary">' 
                    + replaceBadChars(who[1], sens=2) + '</div>';
            }
        html += '</li>';
        }
    html += '</ul>';
    document.getElementById('list-no-appointment').innerHTML = html;

    html = '<ul>';
    for (let i = 0; i < DATA['noResponse'].length; i++)
        html += '<li>' + DATA['USERS'][DATA['noResponse'][i]][2] + '</li>';
    html += '</ul>';
    document.getElementById('list-no-response').innerHTML = html;
    }


function updateEditPage() {
    let who, whoId, whoName;
    let DATA = {};
    for (let i = 0; i < SESSION['profUsers']['allUsers'].length; i++) {
        who = SESSION['profUsers']['allUsers'][i].split('&');
        whoId = who[0];
        whoName = who[1] + ' ' + who[2] + ' (' + who[3] + ')';
        if (SESSION['profUsers']['linkedUsers'].indexOf(whoId) != -1)
            DATA[whoId] = whoName;
        }
    //console.log(DATA);
    let studentsList = '', line;
    for (let i = 0; i < SESSION['profUsers']['linkedUsers'].length; i++) {
        whoId = SESSION['profUsers']['linkedUsers'][i];
        line = '<option value="' + whoId + '">' + DATA[whoId] + '</option>';
        studentsList += line;
        }
    document.getElementById('select-student').innerHTML = studentsList;

    createTimeslotsTable(who='edit');
    }


function updateConfirmationsPage() {
    // page manage-confirmations
    let temp, timestamp;
    let who, whoId, whoName;
    let DATA = {
        'appointments': [], 
        'noAppointment': [], 
        'USERS': {}, 
        'sortedByUsers': [], 
        };
    for (let i = 0; i < SESSION['profAppointments'].length; i++) {
        temp = SESSION['profAppointments'][i].split('|');
        whoId = temp[0];
        DATA['USERS'][whoId] = [temp[1], temp[2]];
        if (parseInt(temp[1].split('@')[0]) != 0) {
            DATA['appointments'].push(whoId);
            timestamp = temp[1].split('@')[0];
            }
        }
    for (let i = 0; i < SESSION['profUsers']['linkedUsers'].length; i++) {
        whoId = SESSION['profUsers']['linkedUsers'][i];
        if (DATA['appointments'].indexOf(whoId) == -1) {
            DATA['USERS'][whoId] = ['', ''];
            }
        else if (DATA['appointments'].indexOf(whoId) > -1)
            DATA['sortedByUsers'].push(whoId);
        }
    for (let i = 0; i < SESSION['profUsers']['allUsers'].length; i++) {
        who = SESSION['profUsers']['allUsers'][i].split('&');
        whoName = who[1] + ' ' + who[2] + ' (' + who[3] + ')';
        if (SESSION['profUsers']['linkedUsers'].indexOf(who[0]) != -1)
            DATA['USERS'][who[0]].push(whoName);
        }

    let profConfirmation = SESSION['profConfirmation'];
    if (profConfirmation.length < 1)
        profConfirmation = SESSION['profMessage'];
    let html = '', space = '&nbsp;&nbsp;&nbsp;';
    for (let i = 0; i < DATA['sortedByUsers'].length; i++) {
        who = DATA['USERS'][DATA['sortedByUsers'][i]];
        temp = who[0].split('@');

        html += '<h3>' + tr('CONFIRMATION OF YOUR APPOINTMENT') + '</h3>';
        html += '<br />';
        html += '<div class="text-start">';
        html += tr('Student:') + space  + '<b>' + who[2] + '</b>';
        html += '<br /><br />';
        html += tr('Madam, Sir,') + '<br />';
        html += space + tr('your appointment with') + space + SESSION['userName'] + space;
        html += tr('is scheduled on') + '<br />' + space;
        html += '<b>' + timestampToText(temp[0]) + '</b>' + space;
        if (temp[1] == 'T')
            html += tr('by phone.')
        else
            html += tr('on site.');
        html += '<br />';
        if (profConfirmation.length > 0) {
            html += '<br />';
            html += '<h4>' + tr("Teacher's message") + '</h4>';
            html += '<div class="alert alert-secondary">' 
                    + replaceBadChars(profConfirmation, sens=2) + '</div>';
            }
        if (who[1].length > 0) {
            html += '<br />';
            html += '<h4>' + tr('Your message') + '</h4>';
            html += '<div class="alert alert-secondary">' 
                    + replaceBadChars(who[1], sens=2) + '</div>';
            }
        html += '</div>';
        html += '<hr />';
        if (i < DATA['sortedByUsers'].length - 1) {
            html += '<p style="page-break-after: always;">&nbsp;</p>';
            html += '<p style="page-break-before: always;">&nbsp;</p>';
            }
        }
    document.getElementById('list-appointments-users').innerHTML = html;
    }


function updateCleanPage() {
    // page manage-clean
    let temp, timestamp;
    let who, whoId, whoName;
    let DATA = {
        'appointments': [], 
        'noAppointment': [], 
        'USERS': {}, 
        'TIMESLOTS': {}, 
        'sortedByTimeslots': [], 
        };
    for (let i = 0; i < SESSION['profAppointments'].length; i++) {
        temp = SESSION['profAppointments'][i].split('|');
        whoId = temp[0];
        DATA['USERS'][whoId] = [temp[1], temp[2]];
        if (parseInt(temp[1].split('@')[0]) == 0)
            DATA['noAppointment'].push(whoId);
        else {
            DATA['appointments'].push(whoId);
            timestamp = temp[1].split('@')[0];
            if (timestamp in DATA['TIMESLOTS'])
                DATA['TIMESLOTS'][timestamp].push(whoId);
            else
                DATA['TIMESLOTS'][timestamp] = [whoId];
            DATA['sortedByTimeslots'].push(timestamp);
            }
        }
    DATA['sortedByTimeslots'].sort();
    for (let i = 0; i < SESSION['profUsers']['linkedUsers'].length; i++) {
        whoId = SESSION['profUsers']['linkedUsers'][i];
        if (DATA['appointments'].indexOf(whoId) == -1) {
            DATA['USERS'][whoId] = ['', ''];
            }
        }
    for (let i = 0; i < SESSION['profUsers']['allUsers'].length; i++) {
        who = SESSION['profUsers']['allUsers'][i].split('&');
        whoName = who[1] + ' ' + who[2] + ' (' + who[3] + ')';
        if (SESSION['profUsers']['linkedUsers'].indexOf(who[0]) != -1)
            DATA['USERS'][who[0]].push(whoName);
        }

    let html = '', line = '';
    html = '<ul class="list-group">';
    // liste des RDV :
    for (let i = 0; i < DATA['sortedByTimeslots'].length; i++) {
        timestamp = DATA['sortedByTimeslots'][i];
        whoId = DATA['TIMESLOTS'][timestamp].shift();
        line = '<li class="list-group-item">';
        line += '<input class="form-check-input me-1" type="checkbox" value="' + whoId + '">';
        who = DATA['USERS'][whoId];
        temp = who[0].split('@');
        if (temp[1] == 'T')
            line += timestampToText(temp[0]) + ' 📞';
        else
            line += timestampToText(temp[0]) + ' 👁';
        line += ' --- ' + who[2];
        line += '</li>';
        html += line;
        }
    // on ajoute les "pas de RDV souhaité" :
    for (let i = 0; i < DATA['noAppointment'].length; i++) {
        whoId = DATA['noAppointment'][i];
        line = '<li class="list-group-item">';
        line += '<input class="form-check-input me-1" type="checkbox" value="' + whoId + '">';
        who = DATA['USERS'][whoId];
        line += tr('No appointment desired');
        line += ' --- ' + who[2];
        line += '</li>';
        html += line;
        }
    html += '</ul>';
    let element = document.getElementById('list-appointments');
    element.innerHTML = html;
    if (element.clientHeight > innerHeight / 2)
        setHeight(element, innerHeight / 2);
    }


function selectAll() {
    // page manage-clean
    [].forEach.call(document.querySelectorAll('input'), function(el) {
        el.checked = true;
        });
    }


function loadProfUsers() {
    // pages manager et manage-users
    let checkeds = [];
    for (let i = 0; i < SESSION['profAppointments'].length; i++)
        checkeds.push(SESSION['profAppointments'][i].split('|')[0]);
    //console.log(checkeds);
    if (PAGE == 'manager') {
        let linksList = '', who, whoName;
        linksList = '<ul>';
        for (let i = 0; i < SESSION['profUsers']['allUsers'].length; i++) {
            who = SESSION['profUsers']['allUsers'][i].split('&');
            whoName = who[1] + ' ' + who[2] + ' (' + who[3] + ')';
            if (SESSION['profUsers']['linkedUsers'].indexOf(who[0]) != -1) {
                if (checkeds.indexOf(who[0]) != -1)
                    linksList += '<li>✔ ' + whoName + '</li>';
                else
                    linksList += '<li>' + whoName + '</li>';
                }
            }
        linksList += '</ul>';
        document.getElementById('list-links').innerHTML = linksList;
        }
    else if (PAGE == 'manage-users') {
        let linksList = '', classesList = '', usersList = '', who, whoName, line;
        linksList = '<ul>';
        usersList = '<ul class="list-group">';
        for (let i = 0; i < SESSION['profUsers']['allUsers'].length; i++) {
            who = SESSION['profUsers']['allUsers'][i].split('&');
            whoName = who[1] + ' ' + who[2] + ' (' + who[3] + ')';
            line = '<li class="list-group-item" what="' + who[3] + '">';
            if (SESSION['profUsers']['linkedUsers'].indexOf(who[0]) != -1) {
                if (checkeds.indexOf(who[0]) != -1)
                    linksList += '<li>✔ ' + whoName + '</li>';
                else
                    linksList += '<li>' + whoName + '</li>';
                line += '<input class="form-check-input me-1" type="checkbox" value="' 
                    + who[0] + '" checked>';
                }
            else
                line += '<input class="form-check-input me-1" type="checkbox" value="' 
                    + who[0] + '">';
            line += whoName;
            line += '</li>';
            usersList += line;
            }
        linksList += '</ul>';
        usersList += '</ul>';

        classesList = '<option class="combobox" selected>' 
            + tr('All classes') + '</option>';
        for (let i = 0; i < SESSION['profUsers']['classes'].length; i++) {
            whoName = SESSION['profUsers']['classes'][i];
            line = '<option value="' + whoName + '">' + whoName + '</option>';
            classesList += line;
            }

        document.getElementById('list-links').innerHTML = linksList;
        document.getElementById('select-class').innerHTML = classesList;
        document.getElementById('list-users').innerHTML = usersList;
        }
    }


function loadProfMessage() {
    // pages manager et manage-message
    let result = SESSION['profMessage'];
    if (PAGE == 'manager') {
        result = replaceBadChars(result, sens=2);
        document.getElementById('messageText').innerHTML = 
                '<div class="alert alert-secondary">' 
                + result 
                + '</div>';
        }
    else if (PAGE == 'manage-message') {
        result = replaceBadChars(result, sens=1);
        document.getElementById('messageText').value = result;
        result = replaceBadChars(SESSION['profConfirmation'], sens=1);
        document.getElementById('confirmationText').value = result;            
        }
    }


function loadTimeslotsText() {
    // pages manager et manage-timeslots
    if (PAGE == 'manager') {
        createTimeslotsTable();
        }
    else if (PAGE == 'manage-timeslots') {
        let result = SESSION['profTimeslotsText'];
        document.getElementById('timeslotsText').value = replaceBadChars(result, sens=1);
        createTimeslotsTable();
        }
    }


function loadUserConfirmation() {
    //console.log('loadUserConfirmation');
    let html = '', 
        profId = SESSION['userData']['profId'], 
        userData = SESSION['userData'][profId], 
        userChoice = SESSION['userChoice'][profId], 
        profName = userData[1], 
        temp = userChoice[2].split('@');
    if (temp[0] == 0) {
        html = tr('You do not want an appointment with');
        html += ' ' + profName + '.';
        }
    else {
        let date = timestampToText(temp[0]);
        html = tr('Your appointment with') + ' ' + profName + ' '; 
        html += tr('is scheduled on') + '<br /><b>' + date + '</b><br />';
        if (temp[1] == 'T')
            html += tr('by phone.')
        else
            html += tr('on site.');
        };
    document.getElementById('user-confirmation').innerHTML = html;
    document.getElementById('user-profMessage').innerHTML = 
        '<div class="alert alert-secondary">' 
        + replaceBadChars(userData[2], sens=2) 
        + '</div>';
    document.getElementById('user-message').innerHTML = 
        '<div class="alert alert-secondary">' 
        + replaceBadChars(userChoice[3], sens=2) 
        + '</div>';
    }






























/* **********************************************
    FONCTIONS QUI INTERROGENT
    LA BASE DE DONNÉES
********************************************** */

function connect() {
    //console.log('connect');
    let login = document.getElementById('login').value, 
        password = document.getElementById('password').value, 
        data = 'what=login&login=' + login + '&password=' + password, 
        url = '';
    if (SESSION['hostname'].includes('clgdrouyn'))
        url =  'https://clgdrouyn.fr/verac/pages/verac_login.php';
    else
        url = LOCAL_URL;

    function doTheJob(result) {
        let user = result;
        //console.log(user);
        SESSION['user'] = user;
        user = user.split('|');
        SESSION['userName'] = 
            '<b>' + user[3] + ' ' + user[4] + '</b>'
            + '&nbsp;&nbsp;&nbsp; (' + user[5] + ')';
        if (user[1] > -1) {
            saveSession();
            window.location = 'user.html';
            }
        else if (user[2] > -1) {
            saveSession();
            window.location = 'manager.html';
            }
        else {
            let msg = '<p></p>';
            msg += '<div class="alert alert-danger text-center" role="alert">';
            msg += '<b>' + tr('Incorrect login or password!') + '</b>';
            msg += '</div>';
            document.getElementById('msg').innerHTML = msg;
            }
        }

    if (window.fetch) {
        fetch(url, {
            method: 'POST', 
            headers: {'Content-type': CONTENT_TYPE}, 
            body: data, 
            })
            .then(response => response.text())
            .then(text => {doTheJob(text)});
        }
    else {
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200)
                doTheJob(this.responseText);
            };
        xmlhttp.open('POST', url);
        xmlhttp.setRequestHeader('Content-Type', CONTENT_TYPE);
        xmlhttp.send(data);
        }
    }


function loadProfData(force=false) {
    if ((force == true) || (SESSION.mustLoadData)) {
        SESSION['profAppointments'] = [];
        SESSION['profUsers'] = {
            'linkedUsers': [], 
            'classes': [], 
            'allUsers': [], 
            };
        SESSION['profMessage'] = '';
        SESSION['profConfirmation'] = '';
        SESSION['profTimeslotsText'] = '';
        let data = 'what=loadProfData&user=' + SESSION['user'];

        function doTheJob(result) {
            result = JSON.parse(result);

            SESSION['profAppointments'] = result['profAppointments'];

            SESSION['profUsers'] = result['users'];
            let checkeds = [];
            for (let i = 0; i < SESSION['profAppointments'].length; i++)
                checkeds.push(SESSION['profAppointments'][i].split('|')[0]);
            let linksList = '', who, whoName;
            linksList = '<ul>';
            for (let i = 0; i < SESSION['profUsers']['allUsers'].length; i++) {
                who = SESSION['profUsers']['allUsers'][i].split('&');
                whoName = who[1] + ' ' + who[2] + ' (' + who[3] + ')';
                if (SESSION['profUsers']['linkedUsers'].indexOf(who[0]) != -1) {
                    if (checkeds.indexOf(who[0]) != -1)
                        linksList += '<li>✔ ' + whoName + '</li>';
                    else
                        linksList += '<li>' + whoName + '</li>';
                    }
                }
            linksList += '</ul>';
            document.getElementById('list-links').innerHTML = linksList;

            SESSION['profMessage'] = result['profMessage'];
            result['profMessage'] = replaceBadChars(result['profMessage'], sens=2);
            document.getElementById('messageText').innerHTML = 
                    '<div class="alert alert-secondary">' 
                    + result['profMessage'] 
                    + '</div>';
            SESSION['profConfirmation'] = result['profConfirmation'];

            SESSION['profTimeslotsText'] = result['timeslotsText'].replace(/ /g, '');
            createTimeslotsTable();

            SESSION.mustLoadData = false;
            saveSession();
            }

        if (window.fetch) {
            fetch(LOCAL_URL, {
                method: 'POST', 
                headers: {'Content-type': CONTENT_TYPE}, 
                body: data, 
                })
                .then(response => response.text())
                .then(text => {doTheJob(text)});
            }
        else {
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200)
                    doTheJob(this.responseText);
                };
            xmlhttp.open('POST', LOCAL_URL);
            xmlhttp.setRequestHeader('Content-Type', CONTENT_TYPE);
            xmlhttp.send(data);
            }
        }
    else {
        loadProfUsers();
        loadProfMessage();
        loadTimeslotsText();
        }
    }


function loadUserData(force=false) {
    if ((force == true) || (SESSION.mustLoadData)) {
        SESSION['userData'] = {'profId': -1, 'profs': [], };
        SESSION['userAppointments'] = {};
        SESSION['userChoice'] = {};
        let data = 'what=loadUserData&user=' + SESSION['user'];

        function doTheJob(result) {
            result = JSON.parse(result);
            SESSION['userData'] = result['userData'];

            let userId, profId, profsList = '', temp, line;
            userId = SESSION['user'].split('|')[1];
            for (let i = 0; i < SESSION['userData']['profs'].length; i++) {
                profId = SESSION['userData']['profs'][i];
                temp = SESSION['userData'][profId];
                line = '<option value="' + temp[0] + '">' + temp[1] + '</option>';
                profsList += line;
                SESSION['userData'][temp[0]][3] = temp[3].replace(/ /g, '');
                SESSION['userChoice'][temp[0]] = [temp[0], userId, '', ''];
                SESSION['userAppointments'][profId] = [];
                }
            document.getElementById('select-prof').innerHTML = profsList;

            profId = SESSION['userData']['profId'];
            if (profId > -1) {
                //console.log('loadUserData NEW : ' + SESSION['userData'][profId]);
                document.getElementById('user-profMessage').innerHTML = 
                    '<div class="alert alert-secondary">' 
                    + replaceBadChars(SESSION['userData'][profId][2], sens=2) 
                    + '</div>';
                for (let i = 0; i < result['userAppointments'].length; i++) {
                    if (result['userAppointments'][i].length > 0) {
                        temp = result['userAppointments'][i].split('|');
                        SESSION['userAppointments'][temp[0]].push(temp[2]);
                        if (temp[1] == SESSION['userChoice'][temp[0]][1]) {
                            SESSION['userChoice'][temp[0]][2] = temp[2];
                            SESSION['userChoice'][temp[0]][3] = temp[3];
                            }
                        }
                    }
                document.getElementById('messageText').value = 
                    replaceBadChars(SESSION['userChoice'][profId][3], sens=1);
                createTimeslotsTable(who='user');
                }
            else
                document.getElementById('btn-save').disabled = true;
            SESSION.mustLoadData = false;
            saveSession();
            }

        if (window.fetch) {
            fetch(LOCAL_URL, {
                method: 'POST', 
                headers: {'Content-type': CONTENT_TYPE}, 
                body: data, 
                })
                .then(response => response.text())
                .then(text => {doTheJob(text)});
            }
        else {
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200)
                    doTheJob(this.responseText);
                };
            xmlhttp.open('POST', LOCAL_URL);
            xmlhttp.setRequestHeader('Content-Type', CONTENT_TYPE);
            xmlhttp.send(data);
            }
        }
    else {
        let profId, profsList = '', temp, line;
        profId = SESSION['userData']['profId'];
        for (let i = 0; i < SESSION['userData']['profs'].length; i++) {
            temp = SESSION['userData'][SESSION['userData']['profs'][i]];
            if (temp[0] == profId)
                line = '<option value="' + temp[0] + '" selected>' + temp[1] + '</option>';
            else
                line = '<option value="' + temp[0] + '">' + temp[1] + '</option>';
            profsList += line;
            if (SESSION['userData']['profId'] < 0)
                SESSION['userData']['profId'] = temp[0];
            SESSION['userData'][temp[0]] = [temp[0], temp[1], temp[2], temp[3]];
            }
        document.getElementById('select-prof').innerHTML = profsList;

        if (profId > -1) {
            document.getElementById('user-profMessage').innerHTML = 
                '<div class="alert alert-secondary">' 
                + replaceBadChars(SESSION['userData'][profId][2], sens=2) 
                + '</div>';
            document.getElementById('messageText').value = replaceBadChars(
                SESSION['userChoice'][profId][3], sens=1);
            createTimeslotsTable(who='user');
            }
        else
            document.getElementById('btn-save').disabled = true;
        }
    }






























/* **********************************************
    FONCTION SAVE
    (ÉCRIT DANS LA BASE DE DONNÉES)
********************************************** */

function save() {
    //console.log('save');
    let data = 'what=submit' 
        + '&page=' + PAGE 
        + '&user=' + SESSION['user'];
    if (PAGE == 'manage-users') {
        let linkedUsers = [];
        [].forEach.call(document.querySelectorAll('.list-group-item'), function(el) {
            let user = el.children[0];
            if (user.checked)
                linkedUsers.push(user.value);
            });
        let params = linkedUsers.join('|');
        data += '&params=' + params;

        function doTheJob(result) {
            SESSION['profUsers']['linkedUsers'] = linkedUsers;
            saveSession();
            loadProfUsers();
            }

        if (window.fetch) {
            fetch(LOCAL_URL, {
                method: 'POST', 
                headers: {'Content-type': CONTENT_TYPE}, 
                body: data, 
                })
                .then(response => response.text())
                .then(text => {doTheJob(text)});
            }
        else {
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200)
                    doTheJob(this.responseText);
                };
            xmlhttp.open('POST', LOCAL_URL);
            xmlhttp.setRequestHeader('Content-Type', CONTENT_TYPE);
            xmlhttp.send(data);
            }
        }
    else if (PAGE == 'manage-timeslots') {
        let params = replaceBadChars(document.getElementById('timeslotsText').value);
        data += '&params=' + params;

        function doTheJob(result) {
            SESSION['profTimeslotsText'] = params;
            saveSession();
            loadTimeslotsText();
            }

        if (window.fetch) {
            fetch(LOCAL_URL, {
                method: 'POST', 
                headers: {'Content-type': CONTENT_TYPE}, 
                body: data, 
                })
                .then(response => response.text())
                .then(text => {doTheJob(text)});
            }
        else {
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200)
                    doTheJob(this.responseText);
                };
            xmlhttp.open('POST', LOCAL_URL);
            xmlhttp.setRequestHeader('Content-Type', CONTENT_TYPE);
            xmlhttp.send(data);
            }
        }
    else if (PAGE == 'manage-message') {
        let profMessage = replaceBadChars(document.getElementById('messageText').value), 
            profConfirmation = replaceBadChars(document.getElementById('confirmationText').value), 
            params = profMessage + '££££$$$$' + profConfirmation;
        data += '&params=' + params;

        function doTheJob(result) {
            SESSION['profMessage'] = profMessage;
            SESSION['profConfirmation'] = profConfirmation;
            saveSession();
            loadProfMessage();

            let msg = '<p></p>';
            msg += '<div class="alert alert-success text-center" role="alert">';
            msg += '<b>' + tr('Changes saved.') + '</b>';
            msg += '</div>';
            document.getElementById('msg').innerHTML = msg;
            setTimeout(function() {
                document.getElementById('msg').innerHTML = '';
                }, 1000);
            }

        if (window.fetch) {
            fetch(LOCAL_URL, {
                method: 'POST', 
                headers: {'Content-type': CONTENT_TYPE}, 
                body: data, 
                })
                .then(response => response.text())
                .then(text => {doTheJob(text)});
            }
        else {
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200)
                    doTheJob(this.responseText);
                };
            xmlhttp.open('POST', LOCAL_URL);
            xmlhttp.setRequestHeader('Content-Type', CONTENT_TYPE);
            xmlhttp.send(data);
            }
        }
    else if (PAGE == 'manage-edit') {
        if (SESSION['temp'].length < 2) 
            return;

        let params = SESSION['temp'];
        let studentId = params[0];
        for (let i = 0; i < SESSION['profAppointments'].length; i++) {
            if (SESSION['profAppointments'][i].split('|')[0] == studentId)
                params.push(SESSION['profAppointments'][i].split('|')[2]);
            }
        if (params.length < 3) 
            params.push('');
        params = params.join('|');
        //console.log(params);
        data += '&params=' + params;

        function doTheJob(result) {
            SESSION['profAppointments'] = JSON.parse(result);
            SESSION['temp'] = [];
            saveSession();

            let msg = '<p></p>';
            msg += '<div class="alert alert-success text-center" role="alert">';
            msg += '<b>' + tr('Changes saved.') + '</b>';
            msg += '</div>';
            document.getElementById('msg').innerHTML = msg;
            setTimeout(function() {
                document.getElementById('msg').innerHTML = '';
                }, 1000);
            }

        if (window.fetch) {
            fetch(LOCAL_URL, {
                method: 'POST', 
                headers: {'Content-type': CONTENT_TYPE}, 
                body: data, 
                })
                .then(response => response.text())
                .then(text => {doTheJob(text)});
            }
        else {
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200)
                    doTheJob(this.responseText);
                };
            xmlhttp.open('POST', LOCAL_URL);
            xmlhttp.setRequestHeader('Content-Type', CONTENT_TYPE);
            xmlhttp.send(data);
            }
        }
    else if (PAGE == 'manage-clean') {
        let params = [];
        [].forEach.call(document.querySelectorAll('input'), function(el) {
            if (el.checked)
                params.push(el.value);
            });
        params = params.join('|');
        //console.log(params);
        data += '&params=' + params;

        function doTheJob(result) {
            SESSION['profAppointments'] = JSON.parse(result);
            saveSession();
            updateCleanPage();
            }

        if (window.fetch) {
            fetch(LOCAL_URL, {
                method: 'POST', 
                headers: {'Content-type': CONTENT_TYPE}, 
                body: data, 
                })
                .then(response => response.text())
                .then(text => {doTheJob(text)});
            }
        else {
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200)
                    doTheJob(this.responseText);
                };
            xmlhttp.open('POST', LOCAL_URL);
            xmlhttp.setRequestHeader('Content-Type', CONTENT_TYPE);
            xmlhttp.send(data);
            }
        }

    else if (PAGE == 'user') {
        let profId = SESSION['userData']['profId'];
        if (SESSION['userChoice'][profId][2].length < 1)
            SESSION['userChoice'][profId][2] = '0';
        SESSION['userChoice'][profId][3] = replaceBadChars(
            document.getElementById('messageText').value);
        SESSION['modified'] = false;
        saveSession();
        let params = SESSION['userChoice'][profId].join('|');
        data += '&params=' + params;

        function doTheJob(result) {
            window.location = 'user-confirmation.html';
            }

        if (window.fetch) {
            fetch(LOCAL_URL, {
                method: 'POST', 
                headers: {'Content-type': CONTENT_TYPE}, 
                body: data, 
                })
                .then(response => response.text())
                .then(text => {doTheJob(text)});
            }
        else {
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200)
                    doTheJob(this.responseText);
                };
            xmlhttp.open('POST', LOCAL_URL);
            xmlhttp.setRequestHeader('Content-Type', CONTENT_TYPE);
            xmlhttp.send(data);
            }
        }

    else if (PAGE == 'password') {
        let password1 = document.getElementById('password-1').value;
        let password2 = document.getElementById('password-2').value;
        let params = password1;
        let msg = '<p></p>';
        if (password1 != password2) {
            msg += '<div class="alert alert-danger text-center" role="alert">';
            msg += '<b>' + tr('The new password does not match the confirmation.') + '</b>';
            msg += '</div>';
            document.getElementById('msg').innerHTML = msg;
            }
        else {
            data += '&params=' + params;

            function doTheJob(result) {
                if (result == 'YES') {
                    msg += '<div class="alert alert-success text-center" role="alert">';
                    msg += '<b>' + tr('The password has been changed.') + '</b>';
                    }
                else {
                    msg += '<div class="alert alert-danger text-center" role="alert">';
                    msg += '<b>' + tr('There was a problem.<br />The change could not be performed.') + '</b>';
                    }
                msg += '</div>';
                document.getElementById('msg').innerHTML = msg;
                }

            if (window.fetch) {
                fetch(LOCAL_URL, {
                    method: 'POST', 
                    headers: {'Content-type': CONTENT_TYPE}, 
                    body: data, 
                    })
                    .then(response => response.text())
                    .then(text => {doTheJob(text)});
                }
            else {
                let xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200)
                        doTheJob(this.responseText);
                    };
                xmlhttp.open('POST', LOCAL_URL);
                xmlhttp.setRequestHeader('Content-Type', CONTENT_TYPE);
                xmlhttp.send(data);
                }
            }
        }
    }






























/* **********************************************
    MISE EN PLACE DE L'INTERFACE
********************************************** */

function init() {
    document.getElementById('webSite').innerHTML = tr('Website');

    if (PAGE != 'index') {
        if (localStorage) {
            let json = sessionStorage.getItem(APPNAME);
            if (json != null)
                SESSION = JSON.parse(json);
            }
        updateNavBar();
        }

    if (PAGE == 'index') {
        // afficher-masquer le mot de passe :
        document.getElementById('btn-showPassword').onmouseover = function() {
            document.getElementById('password').type = 'text';
            };
        document.getElementById('btn-showPassword').onmouseout = function() {
            document.getElementById('password').type = 'password';
            };
        // bouton de connexion :
        document.getElementById('btn-login').addEventListener('click', connect, false);
        // touche entrée :
        document.getElementById('password').addEventListener('keyup', function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                document.getElementById('btn-login').click();
                }
            });
        //if (SESSION['protocol'] == '')
        //    SESSION['protocol'] = window.location.protocol;
        if (SESSION['hostname'] == '')
            SESSION['hostname'] = window.location.hostname;
        // traductions :
        document.title = APPNAME + ' - ' + tr('login');
        document.getElementById('login').setAttribute('placeholder', tr('Login'));
        document.getElementById('password').setAttribute('placeholder', tr('Password'));
        document.getElementById('btn-login').innerHTML = tr('Sign in');
        }

    else if (PAGE == 'manager') {
        [].forEach.call(document.querySelectorAll('div.thumbnail'), function(el) {
            el.addEventListener('click', function() {
                var link = el.children[0].getAttribute('href');
                //console.log('link : ' + link);
                window.location = link;
                })
            });

        // traductions :
        document.title = APPNAME + ' - ' + tr('manager');
        document.getElementById('management-of-appointments').innerHTML = 
            tr('MANAGEMENT OF APPOINTMENTS');
        document.getElementById('list-of-students').innerHTML = tr('List of students');
        document.getElementById('your-message').innerHTML = tr('Your message');
        document.getElementById('time-slots').innerHTML = tr('Time slots');
        document.getElementById('date').innerHTML = tr('Date');
        document.getElementById('students').innerHTML = 
            '<a href="manage-users.html" title="' 
            + tr('Selection of students') + '">' 
            + '<img src="assets/images/users.svg" width="64"><br />' 
            + tr('Students') + '</a>';
        document.getElementById('timeSlots').innerHTML = 
            '<a href="manage-timeslots.html" title="' 
            + tr('Manage available time slots') + '">' 
            + '<img src="assets/images/clock.svg" width="64"><br />' 
            + tr('Time slots') + '</a>';
        document.getElementById('message').innerHTML = 
            '<a href="manage-message.html" title="' 
            + tr('Define a message for parents') + '">' 
            + '<img src="assets/images/message.svg" width="64"><br />' 
            + tr('Message') + '</a>';
        document.getElementById('appointments').innerHTML = 
            '<a href="manage-appointments.html" title="' 
            + tr('See the appointments taken') + '">' 
            + '<img src="assets/images/calendar.svg" width="64"><br />' 
            + tr('Appointments') + '</a>';
        document.getElementById('edit').innerHTML = 
            '<a href="manage-edit.html" title="' 
            + tr('Create or edit appointments') + '">' 
            + '<img src="assets/images/edit.svg" width="64"><br />' 
            + tr('Edit') + '</a>';
        document.getElementById('confirmations').innerHTML = 
            '<a href="manage-confirmations.html" title="' 
            + tr('Print appointment confirmations') + '">' 
            + '<img src="assets/images/print.svg" width="64"><br />' 
            + tr('Confirmations') + '</a>';
        document.getElementById('clean').innerHTML = 
            '<a href="manage-clean.html" title="' 
            + tr('Delete appointments') + '">' 
            + '<img src="assets/images/clear.svg" width="64"><br />' 
            + tr('Clean') + '</a>';
        // récupération des données :
        loadProfData();
        }
    else if (PAGE == 'manage-users') {
        document.getElementById('btn-back').setAttribute('href', 'manager.html');
        // traductions :
        document.title = APPNAME + ' - ' + tr('users');
        document.getElementById('selection-of-students').innerHTML = tr('SELECTION OF STUDENTS');
        document.getElementById('list-of-selected-students').innerHTML = 
            tr('List of selected students');
        // affichage des données :
        loadProfUsers();
        setHeight(document.getElementById('list-users'), innerHeight / 2);
        }
    else if (PAGE == 'manage-timeslots') {
        document.getElementById('btn-back').setAttribute('href', 'manager.html');
        // traductions :
        document.title = APPNAME + ' - ' + tr('timeslots');
        document.getElementById('time-slots').innerHTML = tr('TIME SLOTS');
        document.getElementById('explanations').innerHTML = tr('Explanations');
        let html = '<ul>';
        html += '<li>' + tr('Indicate your time slots in the text box below;') + '</li>';
        html += '<li>' + tr('each line allows you to create one or more slots;') + '</li>';
        html += '<li>' 
            + tr('you can specify at the end of the line if the appointment will be in presence (') 
            + '<strong><span class="citation" data-cites="P">@P</span></strong>' 
            + tr('), by phone (') 
            + '<strong><span class="citation" data-cites="T">@T</span></strong>' 
            + tr(") or at the parents' choice (") 
            + '<strong><span class="citation" data-cites="C">@C</span></strong>' 
            + ').<br />' + tr('By default the appointment will be available in presence only;') + '</li>';
        html += '<li>' + tr('example lines:') + '<ul>';
        html += '<li><strong>03/01/2022-16h00</strong>' 
            + tr(': will create an in-presence appointment slot on Monday 01/03/2022 at 4:00pm') + '</li>';
        html += '<li><strong>03/01/2022-17h00@T</strong>' 
            + tr(': will create a phone appointment on Monday 01/03/2022 at 17h00') + '</li>';
        html += '<li><strong>03/01/2022-17h30@C</strong>' 
            + tr(': will create an appointment slot on Monday 01/03/2022 at 17:30 for parents to choose from (phone or on site)') + '</li>';
        html += '<li><strong>04/01/2022-16h00-16h30-16h45</strong>'  
            + tr(': will create 3 slots on Tuesday 01/04/2022 at the times indicated') + '</li>';
        html += '<li><strong>05/01/2022-16h00:19h00-00h15</strong>' 
            + tr(': will create slots every quarter hour between 4:00pm and 7:00pm.') + '</li>';
        html += '</ul></li>';
        html += '</ul>';
        document.getElementById('explanationsText').innerHTML = html;
        // affichage des données :
        loadTimeslotsText();
        }
    else if (PAGE == 'manage-message') {
        document.getElementById('btn-back').setAttribute('href', 'manager.html');
        // traductions :
        document.title = APPNAME + ' - ' + tr('message');
        document.getElementById('message-to-users').innerHTML = tr('MESSAGE TO USERS');
        document.getElementById('explanations').innerHTML = tr('Explanations');
        let html = tr('You can write a message to parents below.');
        html += '<br />' + tr('The second input field allows you to write a different message for appointment confirmations.');
        html += '<br />' + tr('If you leave it empty the first message will be used.');
        document.getElementById('explanationsText').innerHTML = html;
        // affichage des données :
        loadProfMessage();
        }
    else if (PAGE == 'manage-appointments') {
        document.getElementById('btn-back').setAttribute('href', 'manager.html');
        // traductions :
        document.title = APPNAME + ' - ' + tr('appointments');
        document.getElementById('your-appointments').innerHTML = 
            tr('YOUR APPOINTMENTS');
        document.getElementById('your-appointments-print').innerHTML = 
            APPNAME + ' - ' + SESSION['userName'];
        document.getElementById('appointments-per-student').innerHTML = 
            tr('Appointments (per student)');
        document.getElementById('appointments-per-date').innerHTML = 
            tr('Appointments (per date)');
        document.getElementById('no-appointment-desired').innerHTML = 
            tr('No appointment desired');
        document.getElementById('no-response').innerHTML = 
            tr('No response');
        // affichage des données :
        updateAppointmentsPage();
        }
    else if (PAGE == 'manage-edit') {
        document.getElementById('btn-back').setAttribute('href', 'manager.html');
        // traductions :
        document.title = APPNAME + ' - ' + tr('edit');
        document.getElementById('create-or-edit-appointments').innerHTML = 
            tr('CREATE OR EDIT APPOINTMENTS');
        document.getElementById('explanations').innerHTML = tr('Explanations');
        let html = tr('This page allows you to enter appointments for parents.');
        html += '<br />';
        html += tr('Select a student, edit their appointment and save with the button at the bottom of the page.');
        document.getElementById('explanationsText').innerHTML = html;
        document.getElementById('student').innerHTML = tr('Student');
        // affichage des données :
        updateEditPage();
        }

    else if (PAGE == 'manage-confirmations') {
        document.getElementById('btn-back').setAttribute('href', 'manager.html');
        // traductions :
        document.title = APPNAME + ' - ' + tr('confirmations');
        document.getElementById('appointment-confirmations').innerHTML = 
            tr('APPOINTMENT CONFIRMATIONS');
        document.getElementById('explanationsText').innerHTML = 
            tr('Print this page to get confirmations to distribute.');
        // affichage des données :
        updateConfirmationsPage();
        }
    else if (PAGE == 'manage-clean') {
        document.getElementById('btn-back').setAttribute('href', 'manager.html');
        // traductions :
        document.title = APPNAME + ' - ' + tr('clean');
        document.getElementById('delete-appointments').innerHTML = 
            tr('DELETE APPOINTMENTS');
        document.getElementById('explanations').innerHTML = tr('Explanations');
        document.getElementById('explanationsText').innerHTML = 
            tr('Select the appointments you want to delete '
            + 'and click on the "Clean" button.');
        document.getElementById('btn-selectAll').innerHTML = tr('Select All');
        document.getElementById('btn-selectAll').setAttribute(
            'title', tr('Select all appointments'));
        document.getElementById('btn-selectAll').addEventListener('click', selectAll, false);
        document.getElementById('btn-clean').innerHTML = tr('Clean');
        document.getElementById('btn-clean').setAttribute(
            'title', tr('Delete selected appointments'));
        document.getElementById('btn-clean').addEventListener('click', save, false);
        // affichage des données :
        updateCleanPage();
        }

    else if (PAGE == 'user') {

        window.onbeforeunload = function() {
            if (SESSION['modified'])
                return false;
            };

        document.getElementById('btn-back').style.display = 'none';
        // traductions :
        document.title = APPNAME + ' - ' + tr('user');
        document.getElementById('appointment-booking').innerHTML = 
            tr('APPOINTMENT BOOKING');
        document.getElementById('user-rememberToSave').innerHTML = 
            tr('Remember to save your changes before leaving (button at the bottom of this page).');
        document.getElementById('referring-teacher').innerHTML = tr('Referring teacher');
        document.getElementById('you-can-write-a-message-below').innerHTML = 
            tr('You can write a message below');
        document.getElementById('choose-a-time-slot-for-the-appointment').innerHTML = 
            tr('Choose a time slot for the appointment');
        document.getElementById('date').innerHTML = tr('Date');
        // récupération des données :
        loadUserData();
        }
    else if (PAGE == 'user-confirmation') {
        document.getElementById('btn-back').style.display = '';
        document.getElementById('btn-back').setAttribute('href', 'user.html');
        // traductions :
        document.title = APPNAME + ' - ' + tr('confirmation');
        document.getElementById('confirmation-of-your-appointment').innerHTML = 
            tr('CONFIRMATION OF YOUR APPOINTMENT');
        document.getElementById('teachers-message').innerHTML = tr("Teacher's message");
        document.getElementById('your-message').innerHTML = tr('Your message');
        // affichage des données :
        loadUserConfirmation();
        }
    else if (PAGE == 'password') {
        document.getElementById('btn-back').style.display = '';
        if (SESSION['user'].split('|')[2] < 0)
            document.getElementById('btn-back').setAttribute('href', 'user.html');
        else
            document.getElementById('btn-back').setAttribute('href', 'manager.html');
        document.getElementById('btn-password').style.display = 'none';
        // afficher-masquer le mot de passe :
        document.getElementById('btn-showPassword-1').onmouseover = function() {
            document.getElementById('password-1').type = 'text';
            };
        document.getElementById('btn-showPassword-1').onmouseout = function() {
            document.getElementById('password-1').type = 'password';
            };
        document.getElementById('btn-showPassword-2').onmouseover = function() {
            document.getElementById('password-2').type = 'text';
            };
        document.getElementById('btn-showPassword-2').onmouseout = function() {
            document.getElementById('password-2').type = 'password';
            };

        // traductions :
        document.title = APPNAME + ' - ' + tr('password');
        document.getElementById('change-password').innerHTML = 
            tr('CHANGE PASSWORD');
        document.getElementById('text-1').innerHTML = tr('New password:');
        document.getElementById('text-2').innerHTML = tr('Confirmation:');
        }

    if (SESSION['hostname'] == 'localhost') {
        console.log('page :' + PAGE);
        console.log(SESSION);
        }
    }






























/* **********************************************
    ÉVÉNEMENTS
********************************************** */

window.addEventListener('load', init, false);


// bouton goTop
const goTop = document.getElementById('go-top');
window.addEventListener('scroll', function() {
    if (window.pageYOffset > 250)
        goTop.className = 'active';
    else
        goTop.className = '';
    });

