<?php
/**
#-----------------------------------------------------------------
# This file is a part of RDV project.
# Copyright:    (C) 2022 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/





$what = isset($_REQUEST['what']) ? $_REQUEST['what'] : '';
$login = isset($_REQUEST['login']) ? $_REQUEST['login'] : '';
$password = isset($_REQUEST['password']) ? $_REQUEST['password'] : '';
$user = isset($_REQUEST['user']) ? $_REQUEST['user'] : '|-1|-1||||';
$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : '';
$params = isset($_REQUEST['params']) ? $_REQUEST['params'] : '';










function doEncode($texte, $algo='') {
    /*
    encode un texte en Sha1 ou Sha256 
    (pour mots de passe et logins)
    */
    if ($algo == 'sha256')
        return hash('sha256', $texte);
    elseif ($algo == 'sha1')
        return sha1($texte);
    else
        return [sha1($texte), hash('sha256', $texte)];
    }


function verifyLoginPassword(
        $login, $password, $user_mode) {
    /*
    vérifie les login et mot de passe proposés lors de la connexion.
    */
    $login_enc = doEncode($login);
    $password_enc = doEncode($password);
    $usersPDO = new PDO('sqlite:'.dirname(__FILE__).'/db/users.sqlite');
    $usersPDO -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $usersPDO -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    $SQL = 'SELECT * FROM '.$user_mode.'s ';
    $SQL .= 'WHERE Login IN (:login1, :login256) ';
    $SQL .= 'AND Mdp IN (:mdp1, :mdp256)';
    $OPT = array(
        ':login1' => $login_enc[0], ':login256' => $login_enc[1], 
        ':mdp1' => $password_enc[0], ':mdp256' => $password_enc[1]);
    $STMT = $usersPDO -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    return ($STMT != '') ? $STMT -> fetch() : NULL;
    }










if ($what == 'login') {
    $result = '|-1|-1||||';
    $id_eleve = -1;
    $id_prof = -1;
    $NOM = '';
    $Prenom = '';
    $Matiere = '';
    $record = verifyLoginPassword($login, $password, 'eleve');
    if ($record) {
        $id_eleve = intval($record['id']);
        $NOM = $record['NOM'];
        $Prenom = $record['Prenom'];
        $result = '|'.$id_eleve.'|-1|'
            .$NOM.'|'.$Prenom.'|'
            .$record['Classe'].'|';
        }
    else {
        $record = verifyLoginPassword($login, $password, 'prof');
        if ($record) {
            $id_prof = intval($record['id']);
            $NOM = $record['NOM'];
            $Prenom = $record['Prenom'];
            $result = '|-1|'.$id_prof
                .'|'.$NOM.'|'.$Prenom
                .'|'.$record['Matiere'].'|';
            }
        }
    }

elseif ($what == 'loadUserData') {
    $result = [];
    $result['userData'] = [];
    $result['userAppointments'] = [];
    $user = explode('|', $user);
    $id_eleve = $user[1];
    $id_profs = array();

    $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/rdv.sqlite');
    $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    $SQL = 'SELECT links.id_prof, messages.message, prof_timeslots.timeslots ';
    $SQL .= 'FROM links ';
    $SQL .= 'LEFT JOIN messages ON messages.id_prof=links.id_prof ';
    $SQL .= 'LEFT JOIN prof_timeslots ON prof_timeslots.id_prof=links.id_prof ';
    $SQL .= 'WHERE links.id_eleve=:id_eleve';
    $OPT = array(':id_eleve' => $id_eleve);
    $STMT = $pdo -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $VALUES = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($VALUES as $VALUE) {
        $id_prof = $VALUE['id_prof'];
        $id_profs[] = $id_prof;
        $message = is_null($VALUE['message']) ? '' : $VALUE['message'];
        $timeslots = is_null($VALUE['timeslots']) ? '' : $VALUE['timeslots'];
        $result['userData'][$id_prof] = [$id_prof, '', $message, $timeslots];
        }
    $id_profs = implode(',', $id_profs);

    $SQL = 'SELECT links.id_prof, appointments.id_eleve, ';
    $SQL .= 'appointments.appointment, appointments.message ';
    $SQL .= 'FROM links ';
    $SQL .= 'JOIN appointments ON appointments.id_prof = links.id_prof ';
    $SQL .= 'WHERE links.id_eleve=:id_eleve';
    $OPT = array(':id_eleve' => $id_eleve);
    $STMT = $pdo -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $VALUES = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($VALUES as $VALUE)
        $result['userAppointments'][] = $VALUE['id_prof'].'|'.$VALUE['id_eleve'].'|'.$VALUE['appointment'].'|'.$VALUE['message'];

    $result['userData']['profs'] = [];
    $result['userData']['profId'] = -1;
    $usersPDO = new PDO('sqlite:'.dirname(__FILE__).'/db/users.sqlite');
    $usersPDO -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $usersPDO -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    $SQL = 'SELECT id, NOM, Prenom, Matiere FROM profs ';
    $SQL .= 'WHERE id IN ('.$id_profs.') ';
    $SQL .= 'ORDER BY NOM, Prenom';
    $OPT = array();
    $STMT = $usersPDO -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $VALUES = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($VALUES as $VALUE) {
        $id_prof = $VALUE['id'];
        $result['userData']['profs'][] = $id_prof;
        $result['userData'][$id_prof][1] = $VALUE['NOM'].' '.$VALUE['Prenom'].' ('.$VALUE['Matiere'].')';
        if ($result['userData']['profId'] < 0)
            $result['userData']['profId'] = $id_prof;
        }

    $result = json_encode($result);
    //error_log(print_r($result, True));
    }

elseif ($what == 'loadProfData') {
    $result = [];
    $result['profAppointments'] = [];
    $result['users'] = [];
    $result['profMessage'] = '';
    $result['profConfirmation'] = '';
    $result['timeslotsText'] = '';
    $user = explode('|', $user);
    $id_prof = $user[2];

    // profAppointments :
    $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/rdv.sqlite');
    $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    $SQL = 'SELECT * FROM appointments ';
    $SQL .= 'WHERE id_prof=:id_prof';
    $OPT = array(':id_prof' => $id_prof);
    $STMT = $pdo -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $VALUES = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($VALUES as $VALUE)
        $result['profAppointments'][] = $VALUE['id_eleve'].'|'.$VALUE['appointment'].'|'.$VALUE['message'];

    // users :
    $result['users']['linkedUsers'] = [];
    $SQL = 'SELECT id_eleve FROM links ';
    $SQL .= 'WHERE id_prof=:id_prof';
    $OPT = array(':id_prof' => $id_prof);
    $STMT = $pdo -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $VALUES = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($VALUES as $VALUE)
        $result['users']['linkedUsers'][] = strval($VALUE['id_eleve']);

    $result['users']['classes'] = [];
    $usersPDO = new PDO('sqlite:'.dirname(__FILE__).'/db/users.sqlite');
    $usersPDO -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $usersPDO -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    $SQL = 'SELECT DISTINCT Classe FROM eleves ORDER BY Classe';
    $OPT = array();
    $STMT = $usersPDO -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $VALUES = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($VALUES as $VALUE)
        $result['users']['classes'][] = $VALUE['Classe'];

    $result['users']['allUsers'] = [];
    $SQL = 'SELECT id, NOM, Prenom, Classe FROM eleves ';
    $SQL .= 'ORDER BY Classe, NOM, Prenom';
    $OPT = array();
    $STMT = $usersPDO -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $VALUES = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($VALUES as $VALUE)
        $result['users']['allUsers'][] = implode('&', $VALUE);

    // profMessage et profConfirmation :
    $SQL = 'SELECT * FROM messages ';
    $SQL .= 'WHERE id_prof=:id_prof';
    $OPT = array(':id_prof' => $id_prof);
    $STMT = $pdo -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $VALUES = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($VALUES as $VALUE) {
        $result['profMessage'] = $VALUE['message'];
        $result['profConfirmation'] = $VALUE['confirmation'];
        }

    // timeslotsText :
    $SQL = 'SELECT timeslots FROM prof_timeslots ';
    $SQL .= 'WHERE id_prof=:id_prof';
    $OPT = array(':id_prof' => $id_prof);
    $STMT = $pdo -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $VALUES = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($VALUES as $VALUE)
        $result['timeslotsText'] = $VALUE['timeslots'];

    $result = json_encode($result);
    }




















elseif ($what == 'submit') {
    $result = 'NO';
    $user = explode('|', $user);
    if ($page == 'user')
        $id_eleve = $user[1];
    else
        $id_prof = $user[2];

    if ($page == 'manage-users') {
        $params = explode('|', $params);
        $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/rdv.sqlite');
        $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        try {
            $pdo -> beginTransaction();

            $SQL = 'DELETE FROM links ';
            $SQL .= 'WHERE id_prof=:id_prof';
            $OPT = array(
                ':id_prof' => $id_prof);
            $STMT = $pdo -> prepare($SQL);
            $STMT -> execute($OPT);

            foreach ($params as $id_eleve)
                if (strlen($id_eleve) > 0) {
                    $SQL = 'INSERT INTO links ';
                    $SQL.= 'VALUES(:id_prof, :id_eleve)';
                    $OPT = array(
                        ':id_prof' => $id_prof, 
                        ':id_eleve' => $id_eleve);
                    $STMT = $pdo -> prepare($SQL);
                    $STMT -> execute($OPT);
                    }

            $params = implode(',', $params);
            $SQL = 'DELETE FROM appointments ';
            $SQL .= 'WHERE id_prof=:id_prof ';
            $SQL .= 'AND id_eleve NOT IN ('.$params.')';
            $OPT = array(
                ':id_prof' => $id_prof);
            $STMT = $pdo -> prepare($SQL);
            $STMT -> execute($OPT);

            $pdo -> commit();
            $result = 'YES';
            }
        catch (PDOException $e) {
            $pdo -> rollBack();
            }
        }
    elseif ($page == 'manage-timeslots') {
        $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/rdv.sqlite');
        $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        try {
            $pdo -> beginTransaction();

            $SQL = 'DELETE FROM prof_timeslots ';
            $SQL .= 'WHERE id_prof=:id_prof';
            $OPT = array(
                ':id_prof' => $id_prof);
            $STMT = $pdo -> prepare($SQL);
            $STMT -> execute($OPT);

            if (strlen($params) > 0) {
                $SQL = 'INSERT INTO prof_timeslots ';
                $SQL.= 'VALUES(:id_prof, :timeslots)';
                $OPT = array(
                    ':id_prof' => $id_prof, 
                    ':timeslots' => $params);
                $STMT = $pdo -> prepare($SQL);
                $STMT -> execute($OPT);
                }

            $pdo -> commit();
            $result = 'YES';
            }
        catch (PDOException $e) {
            $pdo -> rollBack();
            }
        }
    elseif ($page == 'manage-message') {
        $params = explode('££££$$$$', $params);
        $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/rdv.sqlite');
        $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        try {
            $pdo -> beginTransaction();

            $SQL = 'DELETE FROM messages ';
            $SQL .= 'WHERE id_prof=:id_prof';
            $OPT = array(
                ':id_prof' => $id_prof);
            $STMT = $pdo -> prepare($SQL);
            $STMT -> execute($OPT);

            if (strlen($params[0].$params[1]) > 0) {
                $SQL = 'INSERT INTO messages ';
                $SQL.= 'VALUES(:id_prof, :message, :confirmation)';
                $OPT = array(
                    ':id_prof' => $id_prof, 
                    ':message' => $params[0], 
                    ':confirmation' => $params[1]);
                $STMT = $pdo -> prepare($SQL);
                $STMT -> execute($OPT);
                }

            $pdo -> commit();
            $result = 'YES';
            }
        catch (PDOException $e) {
            $pdo -> rollBack();
            }
        }
    elseif ($page == 'manage-edit') {
        $result = [];
        $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/rdv.sqlite');
        $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        try {
            $pdo -> beginTransaction();

            $params = explode('|', $params);
            $id_eleve = $params[0];

            $SQL = 'DELETE FROM appointments ';
            $SQL .= 'WHERE id_prof=:id_prof ';
            $SQL .= 'AND id_eleve=:id_eleve';
            $OPT = array(
                ':id_prof' => $id_prof, 
                ':id_eleve' => $id_eleve);
            $STMT = $pdo -> prepare($SQL);
            $STMT -> execute($OPT);

            $SQL = 'INSERT INTO appointments ';
            $SQL.= 'VALUES(:id_prof, :id_eleve, :appointment, :message)';
            $OPT = array(
                ':id_prof' => $id_prof, 
                ':id_eleve' => $id_eleve, 
                ':appointment' => $params[1], 
                ':message' => $params[2]);
            $STMT = $pdo -> prepare($SQL);
            $STMT -> execute($OPT);

            $SQL = 'SELECT * FROM appointments ';
            $SQL .= 'WHERE id_prof=:id_prof';
            $OPT = array(':id_prof' => $id_prof);
            $STMT = $pdo -> prepare($SQL);
            $STMT -> execute($OPT);
            $VALUES = ($STMT != '') ? $STMT -> fetchAll() : array();
            foreach ($VALUES as $VALUE)
                $result[] = $VALUE['id_eleve'].'|'.$VALUE['appointment'].'|'.$VALUE['message'];
            $result = json_encode($result);

            $pdo -> commit();
            }
        catch (PDOException $e) {
            $pdo -> rollBack();
            }
        }
    elseif ($page == 'manage-clean') {
        $result = [];
        $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/rdv.sqlite');
        $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        try {
            $pdo -> beginTransaction();

            $params = explode('|', $params);
            $params = implode(',', $params);
            $SQL = 'DELETE FROM appointments ';
            $SQL .= 'WHERE id_prof=:id_prof ';
            $SQL .= 'AND id_eleve IN ('.$params.')';
            $OPT = array(
                ':id_prof' => $id_prof);
            $STMT = $pdo -> prepare($SQL);
            $STMT -> execute($OPT);

            $SQL = 'SELECT * FROM appointments ';
            $SQL .= 'WHERE id_prof=:id_prof';
            $OPT = array(':id_prof' => $id_prof);
            $STMT = $pdo -> prepare($SQL);
            //$STMT -> setFetchMode(PDO::FETCH_ASSOC);
            $STMT -> execute($OPT);
            $VALUES = ($STMT != '') ? $STMT -> fetchAll() : array();
            foreach ($VALUES as $VALUE)
                $result[] = $VALUE['id_eleve'].'|'.$VALUE['appointment'].'|'.$VALUE['message'];
            $result = json_encode($result);

            $pdo -> commit();
            }
        catch (PDOException $e) {
            $pdo -> rollBack();
            }
        }

    elseif ($page == 'user') {
        $params = explode('|', $params);
        $id_prof = $params[0];

        $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/rdv.sqlite');
        $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        try {
            $pdo -> beginTransaction();

            $SQL = 'DELETE FROM appointments ';
            $SQL .= 'WHERE id_prof=:id_prof ';
            $SQL .= 'AND id_eleve=:id_eleve';
            $OPT = array(
                ':id_prof' => $id_prof, 
                ':id_eleve' => $id_eleve);
            $STMT = $pdo -> prepare($SQL);
            $STMT -> execute($OPT);

            $SQL = 'INSERT INTO appointments ';
            $SQL.= 'VALUES(:id_prof, :id_eleve, :appointment, :message)';
            $OPT = array(
                ':id_prof' => $id_prof, 
                ':id_eleve' => $id_eleve, 
                ':appointment' => $params[2], 
                ':message' => $params[3]);
            $STMT = $pdo -> prepare($SQL);
            $STMT -> execute($OPT);

            $pdo -> commit();
            $result = 'YES';
            }
        catch (PDOException $e) {
            $pdo -> rollBack();
            }
        }

    elseif ($page == 'password') {
        $id_eleve = $user[1];
        $id_prof = $user[2];
        if ($id_prof < 0) {
            $table = 'eleves';
            $user_id = $id_eleve;
            }
        else {
            $table = 'profs';
            $user_id = $id_prof;
            }
        $mdp_enc = doEncode($params, $algo='sha256');

        $usersPDO = new PDO('sqlite:'.dirname(__FILE__).'/db/users.sqlite');
        $usersPDO -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $usersPDO -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        try {
            $usersPDO -> beginTransaction();

            $SQL = 'UPDATE '.$table.' ';
            $SQL .= 'SET Mdp=:mdp ';
            $SQL .= 'WHERE id=:id';
            $OPT = array(
                ':id' => $user_id, 
                ':mdp' => $mdp_enc);
            $STMT = $usersPDO -> prepare($SQL);
            $STMT -> execute($OPT);

            $usersPDO -> commit();
            $result = 'YES';
            }
        catch (PDOException $e) {
            $usersPDO -> rollBack();
            }
        }
    }



echo $result;
?>
