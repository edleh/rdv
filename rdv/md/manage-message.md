---
title: manage-message
---


<main id="content" class="container bg-content border rounded-3">


## MESSAGE TO USERS

----


<div class="text-start">
### Explanations
<div id="explanationsText"></div>
</div>
<p></p>


<div class="row mb-2">
<div class="col-md-7 text-start">
<textarea class="form-control" id="messageText" rows="5" onchange="messageChange()"></textarea>
</div>
<div class="col-md-5 text-start">
<textarea class="form-control" id="confirmationText" rows="5" onchange="messageChange()"></textarea>
</div>
</div>
<p></p>


<div id="msg"></div>
<button type="button" id="btn-save" class="w-100 btn btn-lg btn-primary" title="Save changes">Save</button>


</main>

<br /><br />
