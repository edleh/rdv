---
title: manage-appointments
---


<main id="content" class="container bg-content border rounded-3">


<h2 id="your-appointments" class="d-print-none">YOUR APPOINTMENTS</h2>
<h2 id="your-appointments-print" class="d-none d-print-block">Print Only (Hide on screen only)</h2>

<div id="date"></div>

----


<div class="row mb-2">
<div class="col-md-6 text-start">
### Appointments (per date)
<div id="list-appointments-date"></div>
</div>
<div class="col-md-6 text-start">
### Appointments (per student)
<div id="list-appointments-users"></div>
</div>
</div>

----

<div class="row mb-2">
<div class="col-md-6 text-start">
### No appointment desired
<div id="list-no-appointment"></div>
</div>
<div class="col-md-6 text-start">
### No response
<div id="list-no-response"></div>
</div>
</div>


</main>

<br /><br />
