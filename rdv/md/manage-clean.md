---
title: manage-clean
---


<main id="content" class="container bg-content border rounded-3">


## DELETE APPOINTMENTS

----


<div class="text-start">
### Explanations
<div id="explanationsText"></div>
</div>
<p></p>

<div id="list-appointments" class="text-start scroll-zone"></div>
<p></p>

<div class="row mb-2">
<div class="col-md-6 text-start">
<button type="button" id="btn-selectAll" class="w-100 btn btn-lg btn-primary" title="Select all appointments">Select All</button><p></p>
</div>
<div class="col-md-6 text-start">
<button type="button" id="btn-clean" class="w-100 btn btn-lg btn-primary" title="Delete selected appointments">Clean</button><p></p>
</div>
</div>


</main>

<br /><br />
