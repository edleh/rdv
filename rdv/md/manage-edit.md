---
title: manage-edit
---


<main id="content" class="container bg-content border rounded-3">


## CREATE OR EDIT APPOINTMENTS

----


<div class="text-start">
### Explanations
<div id="explanationsText"></div>
</div>
<p></p>

----


#### Student

<select class="form-select" id="select-student" onchange="studentChange()"></select>

<div class="text-start">
<div id="user-noRDV" class="form-check"></div>
</div>

<table class="table table-striped table-bordered text-center">
<thead>
<tr><th id="date">Date</th><th>👁</th><th>📞</th></tr>
</thead>
<tbody id="timeslots">
</tbody>
</table>

<div id="msg"></div>

<button type="button" id="btn-save" class="w-100 btn btn-lg btn-primary" title="Save changes">Save</button>


</main>

<br /><br />
