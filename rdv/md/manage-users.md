---
title: manage-users
---


<main id="content" class="container bg-content border rounded-3">


## SELECTION OF STUDENTS

----


<div class="row mb-2">

<div class="col-md-6 text-start">
### List of selected students
<div id="list-links"></div>
</div>

<div class="col-md-6">
<div class="input-group">
<select class="form-select" id="select-class" onchange="classChange()">
</select>
</div>
<div id="list-users" class="text-start scroll-zone"></div>
<br />
<button type="button" id="btn-save" class="w-100 btn btn-lg btn-primary" title="Save changes">Save</button>
</div>

</div>


</main>

<br /><br />
