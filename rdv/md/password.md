---
title: password
---


<main id="content" class="container container-index bg-content border rounded-3">
<form id="form-login">


## CHANGE PASSWORD

----



<div id="text-1" class="text-start">New password:</div>
<div class="input-group">
<input type="password" id="password-1" class="form-control" name="password-1">
<div class="input-group-append">
<button type="button" id="btn-showPassword-1" class="btn btn-secondary">&#128065;</button>
</div>
</div>
<p></p>

<div id="text-2" class="text-start">Confirmation:</div>
<div class="input-group">
<input type="password" id="password-2" class="form-control" name="password-2">
<div class="input-group-append">
<button type="button" id="btn-showPassword-2" class="btn btn-secondary">&#128065;</button>
</div>
</div>

<div id="msg"></div>
<p></p>

<button type="button" id="btn-save" class="w-100 btn btn-lg btn-primary" title="Save changes">Save</button>
<p></p>

</form>
</main>


<br /><br />
