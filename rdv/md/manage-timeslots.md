---
title: manage-timeslots
---


<main id="content" class="container bg-content border rounded-3">


## TIME SLOTS

----


<div class="text-start">
### Explanations
<div id="explanationsText"></div>
</div>

<textarea class="form-control" id="timeslotsText" rows="5"></textarea>
</div>
<p></p>

<button type="button" id="btn-save" class="w-100 btn btn-lg btn-primary" title="Save changes">Save</button>
<p></p>


<table class="table table-striped table-bordered">
<thead>
<tr><th>Date</th><th>👁</th><th>📞</th><th>✔</th></tr>
</thead>
<tbody id="timeslots">
</tbody>
</table>


</main>

<br /><br />
