---
title: user-confirmation
---


<main id="content" class="container bg-content border rounded-3">


## CONFIRMATION OF YOUR APPOINTMENT

----

<div id="user-confirmation"></div>

<div class="text-start">
#### Teacher's message

<div id="user-profMessage"></div>


#### Your message

<div id="user-message"></div>
</div>
<p></p>




</main>

<br /><br />
