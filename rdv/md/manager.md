---
title: manager
---


<main id="content" class="container bg-content border rounded-3">


## MANAGEMENT OF APPOINTMENTS

----


<div class="wrapper">

<div id="students" class="thumbnail">
<a href="manage-users.html" title="Selection of students">
<img src="assets/images/users.svg" width="64"><br />Students</a>
</div>

<div id="timeSlots" class="thumbnail">
<a href="manage-timeslots.html" title="Manage available time slots">
<img src="assets/images/clock.svg">Time slots</a>
</div>

<div id="message" class="thumbnail">
<a href="manage-message.html" title="Define a message for parents">
<img src="assets/images/message.svg">Message</a>
</div>

<div id="appointments" class="thumbnail">
<a href="manage-appointments.html" title="See the appointments taken">
<img src="assets/images/calendar.svg">Appointments</a>
</div>

<div id="edit" class="thumbnail">
<a href="manage-edit.html" title="Create or edit appointments">
<img src="assets/images/edit.svg" width="64">Edit</a>
</div>

<div id="confirmations" class="thumbnail">
<a href="manage-confirmations.html" title="Print appointment confirmations">
<img src="assets/images/print.svg">Confirmations</a>
</div>

<div id="clean" class="thumbnail">
<a href="manage-clean.html" title="Delete appointments">
<img src="assets/images/clear.svg">Clean</a>
</div>

</div>


----







<div class="row mb-2">

<div class="col-md-6 text-start">
### List of students
<div id="list-links"></div>
</div>

<div class="col-md-6 text-start">
### Your message
<div id="messageText"></div>

### Time slots
<table class="table table-striped table-bordered text-center">
<thead>
<tr><th id="date">Date</th><th>👁</th><th>📞</th><th>✔</th></tr>
</thead>
<tbody id="timeslots">
</tbody>
</table>
</div>

</div>


</main>

<br /><br />
