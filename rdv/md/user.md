---
title: user
---


<main id="content" class="container bg-content border rounded-3">


## APPOINTMENT BOOKING

<p id="user-rememberToSave">Remember to save your changes before leaving (button at the bottom of this page).</p>


----

#### Referring teacher

<select class="form-select" id="select-prof" onchange="profChange()"></select>

<div id="user-profMessage" class="text-start"></div>


#### You can write a message below

<textarea class="form-control" id="messageText" rows="3" onchange="messageChange()"></textarea>
</div>
<p></p>


#### Choose a time slot for the appointment

<div class="text-start">
<div id="user-noRDV" class="form-check"></div>
</div>

<table class="table table-striped table-bordered text-center">
<thead>
<tr><th id="date">Date</th><th>👁</th><th>📞</th></tr>
</thead>
<tbody id="timeslots">
</tbody>
</table>

<button type="button" id="btn-save" class="w-100 btn btn-lg btn-primary" title="Save changes">Save</button>


</main>

<br /><br />
