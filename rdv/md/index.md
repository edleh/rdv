---
title: connexion
header-includes:
    <!-- azertyuiop -->
footer-includes:
    <!-- qsdfghjklm -->
---

<main id="content" class="container container-index bg-content border rounded-3">
<form id="form-login">

<img class="mb-4" src="assets/images/logo.webp" alt="logo">

<input type="text" id="login" class="form-control" placeholder="Login" name="login">

<div class="input-group">
<input type="password" id="password" class="form-control" placeholder="Password" name="password">
<div class="input-group-append">
<button type="button" id="btn-showPassword" class="btn btn-secondary">&#128065;</button>
<!--<button type="button" id="btn-showPassword" class="btn btn-secondary">
<img src="assets/images/view-visible.svg" width="22">
</button>-->
</div>
</div>
<div id="msg"></div>
<p></p>

<button type="button" id="btn-login" class="w-100 btn btn-lg btn-primary">Sign in</button>
<p></p>

</form>
</main>


<br /><br />
