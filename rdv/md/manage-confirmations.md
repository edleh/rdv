---
title: manage-confirmations
---


<main id="content" class="container bg-content border rounded-3">

<div class="d-print-none">

## APPOINTMENT CONFIRMATIONS

<div id="explanationsText">Print this page to get confirmations to distribute.</div>

----

</div>





<div id="list-appointments-users"></div>





</main>

<br /><br />
