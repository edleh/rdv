#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of RDV project.
# Copyright:    (C) 2022 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------



# importation des modules utiles :
import sys
import os

# récupération du chemin :
HERE = os.path.dirname(os.path.abspath(__file__))
# ajout du chemin au path (+ libs) :
sys.path.insert(0, HERE)
# on démarre dans le bon dossier :
os.chdir(HERE)
FILEDIR = HERE

from xml.dom import minidom
import csv
import zipfile

import random










# pour détecter si on traite les élèves ou les profs :
WHO = ''
WORDS = []

TAG_NAMES = {
    'eleves': (
        'ID_NATIONAL', 
        'NOM_DE_FAMILLE', 'PRENOM', 'DATE_NAISS', 
        'CODE_SEXE', 
        'DATE_ENTREE', 'DATE_SORTIE'), 
    'profs': (
        'NOM_USAGE', 'PRENOM', 'DATE_NAISSANCE', 
        'FONCTION', 'CIVILITE'),     
    }

CSV_FILES_TITLES = {
    'eleves': ('id', 'NOM', 'Prenom', 'Classe', 'Login', 'Mdp'), 
    'profs': ('id', 'NOM', 'Prenom', 'Matiere', 'Login', 'Mdp'), 
    }

MATIERES_NAMES = {
    'ANGLAIS': 'Anglais', 
    'ESPAGNOL': 'Espagnol', 
    'ALLEMAND': 'Allemand', 

    'EDU MUSICA': 'Musique', 
    'ARTS PLAST': 'Arts Plastiques', 
    'E. P. S': 'EPS', 

    'LET MODERN': 'Français', 
    #'LETT CLASS': 'Français', 
    'LETT CLASS': 'Latin', 
    'HIST. GEO.': 'Histoire Géo', 

    'MATHEMATIQ': 'Maths', 
    'PHY.CHIMIE': 'Physique', 
    'S. V. T.': 'SVT', 
    'TECHNOLOGI': 'Technologie', 

    'ULIS TFC': 'ULIS', 

    '': '', 
    }

# 0 : date de naissance, 1 : idem login, 2 : 3 mots au hasard
MDP = 0











def unzipFile(zipFileName, directory):
    """
    d'après 
        http://python.developpez.com/faq/?page=Compression-Archive
    """
    result = [False, '']
    try:
        zfile = zipfile.ZipFile(zipFileName, 'r')
        for name in zfile.namelist():
            if os.path.isdir(name):
                try:
                    os.makedirs(directory + os.sep + name)
                except:
                    pass
            else:
                try:
                    os.makedirs(
                        directory + os.sep + os.path.dirname(name))
                except:
                    pass
                data = zfile.read(name)
                fp = open(directory + os.sep + name, 'wb')
                fp.write(data)
                fp.close()
                result[1] = name
        zfile.close()
        result[0] = True
    finally:
        return result

def doMdp(data):
    if WHO == 'eleves':
        result = data.replace('/', '')
    elif WHO == 'profs':
        if MDP == 1:
            # login :
            result = data[1]
        elif MDP == 2:
            # 3 mots au hasard (xkcd) :
            nb = 3
            result = ' '.join([random.choice(WORDS) for i in range(nb)])
        else:
            # date de naissance :
            text = data[0]
            result = '{0}{1}{2}'.format(text[8:10], text[5:7], text[0:4])
    #print(result)
    return result










def doClasse(text):
    if len(text) == 3:
        return text.replace(' ', '°')
    else:
        return text

def doAscii(text, case='upper'):
    REPLACEMENTS = {
        'A': 'ÁÀÂÃÄÅáàâãäå',
        'AE': 'Ææ', 
        'C': 'Çç', 
        'E': 'ÉÈÊËéèêë', 
        'I': 'ÍÌÎÏíìîï', 
        'N': 'Ññ', 
        'O': 'ÓÒÔÕÖóòôõö', 
        'OE': 'Œœ', 
        'SS': 'ẞß', 
        'U': 'ÚÙÛÜúùûü', 
        'Y': 'ÝŸýÿ', 
        '': '°', 
        }
    result = text
    for replaceWith in REPLACEMENTS:
        for replaceWhat in REPLACEMENTS[replaceWith]:
            result = result.replace(replaceWhat, replaceWith)
    if case == 'upper':
        result = result.upper()
    elif case == 'lower':
        result = result.lower()
    return result

def calculeLogin(prenom, nom):
    """
    calcul du login de l'élève d'après la procédure
    utilisée dans Scribe (histoire d'avoir le même login).
    Modifications : tant pis pour le collage à Scribe (leur procédure a trop de défauts).
    """
    def simplifie(inText):
        """
        pour virer 2 3 trucs qui n'ont rien à faire dans les logins
        """
        newText = inText
        newText = newText.replace('\t', '')
        newText = newText.replace('\n', '')
        newText = newText.replace('\v', '')
        newText = newText.replace('\f', '')
        newText = newText.replace('\r', '')
        newText = newText.replace('-', ' ')
        newText = newText.replace("'", ' ')
        return newText

    def createName(names=[]):
        """
        on essaye de ne pas couper un morceau
        """
        name = ''
        encore = True
        if len(names) < 1:
            # nom ou prénom vide (ça existe ?)
            encore = False
        while encore:
            nextName = names.pop(0)
            if len(nextName) > 15:
                # on coupe le morceau :
                nextName = nextName[:15]
            if len(name) < 8:
                name = '{0}{1}'.format(name, nextName)
            if len(names) < 1:
                encore = False
            if len(name) > 10:
                # 10 caractères suffisent
                encore = False
        if len(name) > 15:
            # on coupe :
            name = name[:15]
        return name

    login = ''
    # on récupère la liste des prénoms et des noms :
    prenoms = simplifie(prenom).split()
    noms = simplifie(nom).split()
    # en minuscules et sans accents :
    for p in prenoms:
        prenoms[prenoms.index(p)] = doAscii(p, case='lower')
    for n in noms:
        noms[noms.index(n)] = doAscii(n, case='lower')
    # un seul mot pour chacun et limité en longueur :
    prenom = createName(prenoms)
    nom = createName(noms)
    # on peut créer le login (avec une dernière limite) :
    login = prenom + '.' + nom
    login = login[:30]
    return login



















def xml2Dic(fileName):
    """
    mise à jour des élèves à partir du fichier SIECLE
    """
    print('xml2Dic', WHO)
    result = [False, '', {}]
    doc = minidom.parse(fileName)

    # vérification du fichier :
    xmlOK = None
    if WHO == 'eleves':
        xmlOK = doc.getElementsByTagName('BEE_ELEVES')
    elif WHO == 'profs':
        xmlOK = doc.getElementsByTagName('STS_EDT')
    if (xmlOK == None) or (xmlOK.length < 1):
        result[1] = 'The XML file provided does not seem to be the right one.'
        return result

    if WHO == 'eleves':
        dataInXML = {}

        elevesList = doc.getElementsByTagName('ELEVE')
        for eleve in elevesList:
            id_eleve = int(eleve.getAttribute('ELENOET'))
            dataInXML[id_eleve] = {}
            dataInXML[id_eleve]['Classe'] = '???'
            for tagName in TAG_NAMES[WHO]:
                element = eleve.getElementsByTagName(tagName)
                data = ''
                if element.length > 0:
                    data = element[0].firstChild.data
                dataInXML[id_eleve][tagName] = data

        # on cherche les classes :
        structuresEleveList = doc.getElementsByTagName('STRUCTURES_ELEVE')
        for structureEleve in structuresEleveList:
            id_eleve = int(structureEleve.getAttribute('ELENOET'))
            codeStructure = structureEleve.getElementsByTagName('CODE_STRUCTURE')
            codeStructure = codeStructure[0].firstChild.data
            typeStructure = structureEleve.getElementsByTagName('TYPE_STRUCTURE')
            typeStructure = typeStructure[0].firstChild.data
            if typeStructure == 'D':
                dataInXML[id_eleve]['Classe'] = doClasse(codeStructure)

        result[0] = True
        result[2] = dataInXML

    elif WHO == 'profs':
        """
        L'import des profs se fait avec le fichier sts_emp_RNE_aaaa.xml
        où RNE est le n° de RNE de l'établissement et aaaa l'année (septembre).
        Par exemple, à VÉRAC pour 2011-2012, le fichier s'appelle sts_emp_0332706M_2011.xml.
        Mais le fichier contient aussi :
            * les classes
            * les matières
        """
        dataInXML = {}
        matieresList = []
        # RÉCUPÉRATION DES PROFS ET MATIÈRES
        profsList = doc.getElementsByTagName('INDIVIDU')
        for prof in profsList:
            id_prof = int(prof.getAttribute('ID'))
            dataInXML[id_prof] = {}
            profType = prof.getAttribute('TYPE')
            for tagName in TAG_NAMES[WHO]:
                element = prof.getElementsByTagName(tagName)
                data = ''
                if element.length > 0:
                    data = element[0].firstChild.data
                dataInXML[id_prof][tagName] = data
            # on ne gardera que la dernière matière s'il y en a plusieurs :
            dataInXML[id_prof]['Matiere'] = ''
            matiereXmlName = ''
            profMatieresList = prof.getElementsByTagName('DISCIPLINE')
            for matiereElement in profMatieresList:
                if matiereXmlName != '':
                    print(dataInXML[id_prof]['NOM_USAGE'], matiereXmlName)
                matiereXmlName = matiereElement.getElementsByTagName('LIBELLE_COURT')
                matiereXmlName = matiereXmlName[0].firstChild.data
            dataInXML[id_prof]['DISCIPLINE'] = matiereXmlName
            if matiereXmlName != '':
                dataInXML[id_prof]['Matiere'] = MATIERES_NAMES.get(matiereXmlName, matiereXmlName)
                if not(matiereXmlName in matieresList):
                    matieresList.append(matiereXmlName)
            elif dataInXML[id_prof]['FONCTION'] == 'EDU':
                dataInXML[id_prof]['Matiere'] = 'Vie scolaire'
            elif dataInXML[id_prof]['FONCTION'] == 'DIR':
                dataInXML[id_prof]['Matiere'] = 'Direction'
        print(matieresList)

        result[0] = True
        result[2] = dataInXML

    return result


def dic2Csv(dic):
    print('dic2Csv', WHO)
    #print(dic)
    try:
        fileName = '{0}{1}{2}.csv'.format(FILEDIR, os.sep, WHO)
        print(fileName)
        theFile = open(fileName, 'w', newline='', encoding='utf-8')
        writer = csv.writer(theFile, delimiter=';', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        titles = []
        titles.extend(CSV_FILES_TITLES[WHO])
        titles.extend(TAG_NAMES[WHO])
        if WHO == 'profs':
            titles.append('DISCIPLINE')
        #print(titles)
        writer.writerow(titles)

        if WHO == 'eleves':
            for id_eleve in dic:
                nom = dic[id_eleve]['NOM_DE_FAMILLE']
                prenom = dic[id_eleve]['PRENOM']
                login = calculeLogin(prenom, nom)
                mdp = doMdp(data=dic[id_eleve]['DATE_NAISS'])
                
                mustDo = True
                if dic[id_eleve]['Classe'] == '???':
                    mustDo = False
                elif 'DATE_SORTIE' in dic[id_eleve]:
                    if len(dic[id_eleve]['DATE_SORTIE']) > 0:
                        mustDo = False
                if mustDo:
                    row = [
                        id_eleve, 
                        nom, 
                        prenom, 
                        dic[id_eleve]['Classe'], 
                        login, 
                        mdp, 
                        ]
                    for tagName in TAG_NAMES[WHO]:
                        row.append(dic[id_eleve][tagName])
                    #print(row)
                    writer.writerow(row)
        elif WHO == 'profs':
            #print(dic[21009])
            for id_prof in dic:
                nom = dic[id_prof]['NOM_USAGE']
                prenom = dic[id_prof]['PRENOM']
                login = calculeLogin(prenom, nom)
                mdp = doMdp(data = (dic[id_prof]['DATE_NAISSANCE'], login, ))

                row = [
                    id_prof, 
                    nom, 
                    prenom, 
                    dic[id_prof]['Matiere'], 
                    login, 
                    mdp, 
                    ]
                for tagName in TAG_NAMES[WHO]:
                    row.append(dic[id_prof][tagName])
                row.append(dic[id_prof]['DISCIPLINE'])
                writer.writerow(row)

    except:
        print('rrrrrrrrrrrrrrrrrrr')
    finally:
        theFile.close()











def main(fileName):
    global WHO, FILEDIR, WORDS, MDP
    print('fileName :', fileName)
    FILEDIR = os.path.dirname(os.path.abspath(fileName))

    if ('ElevesSansAdresses' or 'ElevesAvecAdresses') in fileName:
        WHO = 'eleves'
    elif 'sts_emp' in fileName:
        WHO = 'profs'
        WORDS = [word.rstrip('\n') for word in open('wordlist_fr_4k.txt')]
    else:
        print('mauvais fichier')
        return
    print(WHO)

    # recherche d'un MDP passé en argument (par exemple MDP=3) :
    for arg in sys.argv:
        if arg.split('=')[0] == 'MDP':
            try:
                MDP = int(arg.split('=')[1])
            except:
                MDP = 0
    #print('MDP :', MDP, type(MDP))

    dataInXML = {}
    if fileName[-4:] == '.zip':
        result = unzipFile(fileName, FILEDIR)
        fileName = fileName.replace('.zip', '.xml')
    if fileName[-4:] == '.xml':
        [ok, msg, dataInXML] = xml2Dic(fileName)
        if not(ok):
            print(msg)
            return
        dic2Csv(dataInXML)





if __name__ == '__main__':
    #print(sys.argv, len(sys.argv))
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        print('manque le fichier')

