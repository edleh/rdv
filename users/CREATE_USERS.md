<style>
div.center {
text-align: center;
}
</style>





# RDV
## <small>Créer les utilisateurs. - Create users.</small>



Les comptes des 2 types d'utilisateurs (profs et parents) sont inscrits dans la base de données **rdv/assets/db/users.sqlite**.  
Les parents utilisent donc un compte au nom de l'élève qu'ils représentent.

Des utilisateurs existent déjà pour tester l'application mais vous aurez besoin de les modifier :

<div class="center" align="center">

![](images/CREATE_USERS-13.png) ![](images/CREATE_USERS-12.png)
</div>

Pour ce faire :

* modifiez les fichiers **rdv/assets/db/eleves.csv** et **rdv/assets/db/profs.csv** en y inscrivant vos utilisateurs.  
Chacun correspond à une table de la base de donnée.  
Si vous les modifiez avec [LibreOffice](https://fr.libreoffice.org/), utilisez les réglages suivants :

<div class="center" align="center">

![](images/CREATE_USERS-11.png)
</div>

* remarques concernant les champs :
    * **id** : un identifiant (numéro) qui doit être unique
    * **Login** et **Mdp** : ces 2 champs seront chiffrés avant d'être inscrits dans la base de donnée
* utilisez ensuite le script [Python](https://www.python.org/) **rdv/assets/db/csv2sqlite.py** en lui passant l'argument **eleves** ou **profs** suivant la table que vous voulez créer.  
Par exemple pour créer la table **eleves** :

<div class="center" align="center">

![](images/CREATE_USERS-20.png)
</div>







----

**PAGE À FINIR**

* modifier le script pour pouvoir aussi ajouter des utilisateurs (sans suppression)
* mais aussi mettre à jour sans changer login et mdp (par exemple changement de classe ; donc se baser sur l'id)



