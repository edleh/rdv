#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of RDV project.
# Copyright:    (C) 2022 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------



# importation des modules utiles :
import sys
import os

# récupération du chemin :
HERE = os.path.dirname(os.path.abspath(__file__))
# ajout du chemin au path (+ libs) :
sys.path.insert(0, HERE)
# on démarre dans le bon dossier :
os.chdir(HERE)

import csv
import hashlib
import sqlite3




def csv2List(fileName):
    """
    lit un fichier csv et retourne son contenu sous forme de 2 listes :
        headers contient les titres (première ligne du fichier)
        data contient les autres lignes.
    """
    headers, data = [], []
    # pour gérer l'encodage du fichier :
    encodings = ('utf-8', 'iso-8859-15', 'iso-8859-1')
    for enc in encodings:
        headers, data = [], []
        firstRow = True
        theFile = open(fileName, newline='', encoding=enc)
        try:
            reader = csv.reader(
                theFile, 
                delimiter=';', 
                quotechar='"', 
                quoting=csv.QUOTE_NONNUMERIC)
            for row in reader:
                newRow = []
                for item in row:
                    if item in ('', None):
                        newRow.append('')
                    elif isinstance(item, (int, float)):
                        newRow.append(item)
                    else:
                        newRow.append(item)
                if firstRow:
                    headers = newRow
                    firstRow = False
                else:
                    data.append(newRow)
        except:
            continue
        finally:
            theFile.close()
        break
    return headers, data


def doEncode(texte):
    """
    encode un texte en Sha256 
    (pour mots de passe et logins)
    """
    enc = hashlib.sha256(texte.encode('utf-8')).hexdigest()
    return enc






def main(table='eleves'):
    if not(table in ('eleves', 'profs')):
        table = 'eleves'
    print('table :', table)
    return

    fileName = '{0}.csv'.format(table)
    headers, data = csv2List(fileName)
    #print(headers)
    #print(data)
    dataForDB = []
    for row in data:
        #print(row[4], doEncode(row[4]))
        #print(row)
        dataForDB.append(
            (round(row[0]), row[1], row[2], row[3], doEncode(row[4]), doEncode(row[5]))
            )
    #print(dataForDB)

    con = sqlite3.connect('users.sqlite')
    cur = con.cursor()

    #cur.execute('DROP TABLE {0}'.format(table))
    cur.execute('DELETE FROM {0}'.format(table))
    if table == 'eleves':
        cur.execute(
            '''CREATE TABLE IF NOT EXISTS eleves 
            (id INTEGER PRIMARY KEY, 
            NOM TEXT, Prenom TEXT, Classe TEXT, 
            Login TEXT, Mdp TEXT)''')
    else:
        cur.execute(
            '''CREATE TABLE IF NOT EXISTS profs 
            (id INTEGER PRIMARY KEY, 
            NOM TEXT, Prenom TEXT, Matiere TEXT, 
            Login TEXT, Mdp TEXT)''')
    cur.executemany(
        'INSERT INTO {0} VALUES (?, ?, ?, ?, ?, ?)'.format(table), 
        dataForDB)

    con.commit()
    cur.execute('VACUUM')
    con.close()











if __name__ == '__main__':
    #print(sys.argv, len(sys.argv))
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        main()








